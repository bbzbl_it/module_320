# Code repository for Module 320

## Structure
```
Module_320
├── Code
│   ├── Woche_1
│   ├── Woche_2
│   └── ...
├── Documentation
│   ├── Lerntagebuch
│   │   ├── Woche 1.md
│   │   ├── Woche 2.md
│   │   └── ...
│   └── Recourcen
│       ├── 0_CODE.md
│       ├── 1_Klassen und Objekte.md
│       ├── 2_Kapselung.md
│       ├── ...
│       └── Files
│           ├── 1_Klassen und Objekte
│           │   └── ...
│           ├── 2_Kapselung
│           │   └── ...
│           ├── 3_Konstruktor
│           │   └── ...
│           ├── 4_UML
│           │   └── ...
│           ├── 5_Mehrere Fachklassen
│           │   └── ...
│           ├── 6_ArrayList<T>
│           │   └── ...
│           └── 7_Modellieren und Implementieren
│               └── ...
└── README.md

```
