# Kapselung

Bei der Kapselung handelt es sich um ein Konzept in der Objektorientierten Softwareentwicklung, sodass z.B. der Zugriff auf die Daten eines Objekts kontrolliert sicher gestellt werden kann.



## Kapselung

Um sicherzustellen, dass der Zustand eines Objekts nicht von überall her beliebig verändert werden kann, wendet man in der objektorientierten Programmierung das Konzept der Kapselung an.


### Auftrag

Nach dem Durcharbeiten des Skripts, beantworten Sie die Fragen des Auftrags direkt in Ihr ePortfolio.

[[M320_W01_4_Auftrag_Kapselung.pdf|M320_W01_4_Auftrag_Kapselung]]


### Lösung:

[[M320_W01_4_Auftrag_Kapselung_GELOEST.pdf]]


### Skript

[[M320_W01_4_Skript_Kapselung.pdf]]



## Kapselung beurteilen

Versuchen Sie, die Kapselung anhand folgendem Beispiel zu beurteilen. Schauen Sie sich die erweiterte Account Klasse an und beantworten Sie die Fragen.

[[M320_W02_1_Kapselung_beurteilen.pdf]]

```markdown
Wurde die Kapselung gut umgesetzt? Woran erkennen Sie dies?
-----------
Ja, die Kapselung wurde gut umgesetzt. Das bedeutet, dass die inneren Informationen der Klasse "Account" geschützt sind und nicht direkt von außerhalb geändert werden können.


Wie könnte von einem Account Objekt der Kontostand abgefragt werden? (getter)
-----------
Account.getBalance();

Wie könnte der Zinssatz auf einem Account Objekt verändert werden? (setter)
-----------
Account.setInterestRate(0.01)

```