# Mehrere Fachklassen

Bisher haben Sie eine Fachklasse erstellt und dabei mit Objekten dieser einen Klasse gearbeitet. Hier werden nun mehrere Fachklassen benötigt, wobei Fachklassen Variablen vom Typ von anderen eigenen Fachklassen beinhalten.



## Arbeitsblatt Regatta

Eine ausgereifte Software besteht oft aus sehr vielen Fachklassen. Sie haben zuvor ein Beispiel mit einer Fachklasse gelöst. Wir wollen dies um eine weitere Fachklasse erweitern. Diese Aufgabe bildet auch die Basis für weitere Aufgaben.

[[M320_W02_4_Regatta.pdf]]


### Lösung:

[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_2/SailingRace)


### Musterlösung:

`Start.java`
```java
/**
 * Testklasse
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Start {

	/**
	 * Startmethode
	 * @param args
	 */
	public static void main(String[] args) {
		
		Competition c = new Competition("Rotsee Regatta");
		
		Ship ship1 = new Ship(1, "Alinghi");
		Ship ship2 = new Ship(2, "Red Baron");
		Ship ship3 = new Ship(3, "Blue Lagoon");
		
		c.addShip(ship1);
		c.addShip(ship2);
		c.addShip(ship3);
		
		c.start();
	
		c.printResult();
	}	
}
```

`Ship.java`
```java
import java.util.Random;

/**
 * Bietet Funktionen eines Schiffs an
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Ship {

	private int nr;
	private String name;
	private int time;

	public Ship(int nr, String name) {
		this.nr = nr;
		this.name = name;
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	/**
	 * 
	 * @return Name des Schiffs
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setzt den Namen des Schiffs neu
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Führt das Rennen durch und berechnet die Rennzeit.
	 * Speichert dieses Ergebnis in time
	 */
	public void race() {
		int min = 300;
		int max = 600;
		this.time = (int) (Math.random() * (max - min + 1)) + min;
	}
	
	/**
	 * Gibt die Rennzeit des Schiffs zurück
	 * @return
	 */
	public int getTime() {
		return time;
	}
	
}
```

`Competition.java`
```java
/**
 * Klasse, um Wettkämpfe mit Schiffen zu absolvieren
 * 
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Competition {

	private String name;
	private Ship[] ships = new Ship[5];

	/**
	 * Erstellt ein neues Wettkampf Objekt
	 * 
	 * @param name Name des Wettkampfs
	 */
	public Competition(String name) {
		this.name = name;
	}

	/**
	 * Startet den Wettkampf.
	 * 
	 * @see Ship#race()
	 */
	public void start() {

		for (Ship s : this.ships) {
			if (s != null) {
				s.race();
			}
		}

		// Alternative:
		/*
		 * for (int i = 0; i < ships.length; i++) { 
		 * 	if (ships[i] != null) { 
		 * 		Ship s = ships[i]; 
		 * 		s.race(); 
		 *  }
		 * }
		 */
	}

	/**
	 * Fügt ein Schiff zum Wettkampf hinzu, sofern noch nicht alle Plätze belegt
	 * sind.
	 * 
	 * @param s Ship
	 */
	public void addShip(Ship ship) {
		for (int i = 0; i < ships.length; i++) {
			if (ships[i] == null) {
				ships[i] = ship;
				break;
			}
		}
	}

	/**
	 * Gibt alle Schiffe des Wettkampfs auf die Konsole aus
	 */
	public void printResult() {
		System.out.println("Wettkampf: " + this.name);

		for (Ship s : this.ships) {
			if (s != null) {
				System.out.println("Schiff Nr: " + s.getNr() + " Name: " + s.getName() + " Zeit: " + s.getTime());
			}
		}
	}
}
```



## foreach-Schleife

Mit einer foreach-Schleife kann nicht nur über Arrays von primitiven Typ, sondern auch von Referenzdatentypen iteriert werden.



## Erweiterung um eine neue Fachklasse

Um auch sichtbar zu machen, dass Sie ein einzelnes Objekt in eine Instanzvariable speichern können, behandeln wir folgende Erweiterung zum Beispiel Regatta:

[[M320_W02_4_Regatta_Erweiterung.pdf]]


### Lösung:

[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_2/SailingRaceAdvanced)


### Musterlösung:

`Start.java`
```java
/**
 * Testklasse
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Start {

	/**
	 * Startmethode
	 * @param args
	 */
	public static void main(String[] args) {
		
		Competition c = new Competition("Rotsee Regatta");
		
		Person owner = new Person("Roger", "Zaugg", "Hölstein");
		Ship ship1 = new Ship(1, "Alinghi");
		Ship ship2 = new Ship(2, "Red Baron");
		Ship ship3 = new Ship(3, "Blue Lagoon");
		
		ship1.setOwner(owner);
		ship2.setOwner(owner);
		ship3.setOwner(owner);
		
		c.addShip(ship1);
		c.addShip(ship2);
		c.addShip(ship3);
		
		c.start();
	
		c.printResult();
	}	
}
```

`Person.java`
```java
public class Person {

	private String firstname;
	private String lastname;
	private String residence;
	
	public Person(String firstname, String lastname, String residence) {
		this.firstname = firstname;
		this.lastname = lastname;
		this.residence = residence;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getResidence() {
		return residence;
	}

	public void setResidence(String residence) {
		this.residence = residence;
	}
}
```

`Ship.java`
```java
import java.util.Random;

/**
 * Bietet Funktionen eines Schiffs an
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Ship {

	private int nr;
	private String name;
	private int time;
	private Person owner;

	public Ship(int nr, String name) {
		this.nr = nr;
		this.name = name;
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	/**
	 * 
	 * @return Name des Schiffs
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setzt den Namen des Schiffs neu
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Führt das Rennen durch und berechnet die Rennzeit.
	 * Speichert dieses Ergebnis in time
	 */
	public void race() {
		int min = 300;
		int max = 600;
		this.time = (int) (Math.random() * (max - min + 1)) + min;
	}
	
	/**
	 * Gibt die Rennzeit des Schiffs zurück
	 * @return
	 */
	public int getTime() {
		return time;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}
	
}
```

`Competition.java`
```java
/**
 * Klasse, um Wettkämpfe mit Schiffen zu absolvieren
 * 
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Competition {

	private String name;
	private Ship[] ships = new Ship[5];

	/**
	 * Erstellt ein neues Wettkampf Objekt
	 * 
	 * @param name Name des Wettkampfs
	 */
	public Competition(String name) {
		this.name = name;
	}

	/**
	 * Startet den Wettkampf.
	 * 
	 * @see Ship#race()
	 */
	public void start() {

		for (Ship s : this.ships) {
			if (s != null) {
				s.race();
			}
		}

		// Alternative:
		/*
		 * for (int i = 0; i < ships.length; i++) { if (ships[i] != null) { Ship s =
		 * ships[i]; s.race(); } }
		 */
	}

	/**
	 * Fügt ein Schiff zum Wettkampf hinzu, sofern noch nicht alle Plätze belegt
	 * sind.
	 * 
	 * @param s Ship
	 */
	public void addShip(Ship ship) {
		for (int i = 0; i < ships.length; i++) {
			if (ships[i] == null) {
				ships[i] = ship;
				break;
			}
		}
	}

	/**
	 * Gibt alle Schiffe des Wettkampfs auf die Konsole aus
	 */
	public void printResult() {
		System.out.println("Wettkampf: " + this.name);

		for (Ship s : this.ships) {
			if (s != null) {
				String ownerOutput = " Besitzer: ";

				Person owner = s.getOwner();
				if (owner != null) {
					ownerOutput += owner.getFirstname() + " " + owner.getLastname();
				}

				System.out.println(
						"Schiff Nr: " + s.getNr() + " Name: " + s.getName() + ownerOutput + " Zeit: " + s.getTime());
			}
		}
	}
}
```



## Zusatz: Aufgaben

[[M320_W02_4_Regatta_Zusatzaufgaben.pdf]]


### Lösung:

[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_3/SortShips)


### Musterlösung

`Start.java`
```java
/**
 * Testklasse
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Start {

	/**
	 * Startmethode
	 * @param args
	 */
	public static void main(String[] args) {
		
		Competition c = new Competition("Rotsee Regatta");
		
		Ship ship1 = new Ship(1, "Alinghi");
		Ship ship2 = new Ship(2, "Red Baron");
		Ship ship3 = new Ship(3, "Blue Lagoon");
		
		c.addShip(ship1);
		c.addShip(ship2);
		c.addShip(ship3);
		
		c.start();
	
		c.printResult();
	}	
}
```

`Ship.java`
```java
import java.util.Comparator;
import java.util.Random;

/**
 * Bietet Funktionen eines Schiffs an
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Ship {

	private int nr;
	private String name;
	private int time;

	public Ship(int nr, String name) {
		this.nr = nr;
		this.name = name;
	}

	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	/**
	 * 
	 * @return Name des Schiffs
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setzt den Namen des Schiffs neu
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Führt das Rennen durch und berechnet die Rennzeit.
	 * Speichert dieses Ergebnis in time
	 */
	public void race() {
		int min = 300;
		int max = 600;
		this.time = (int) (Math.random() * (max - min + 1)) + min;
	}
	
	/**
	 * Gibt die Rennzeit des Schiffs zurück
	 * @return
	 */
	public int getTime() {
		return time;
	}
}
```

`Competition.java`
```java
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Klasse, um Wettkämpfe mit Schiffen zu absolvieren
 * 
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Competition {

	private String name;
	private Ship[] ships = new Ship[5];

	/**
	 * Erstellt ein neues Wettkampf Objekt
	 * 
	 * @param name Name des Wettkampfs
	 */
	public Competition(String name) {
		this.name = name;
	}

	/**
	 * Startet den Wettkampf.
	 * 
	 * @see Ship#race()
	 */
	public void start() {

		for (Ship s : this.ships) {
			if (s != null) {
				s.race();
			}
		}

		// Alternative:
		/*
		 * for (int i = 0; i < ships.length; i++) { if (ships[i] != null) { Ship s =
		 * ships[i]; s.race(); } }
		 */
	}

	/**
	 * Fügt ein Schiff zum Wettkampf hinzu, sofern noch nicht alle Plätze belegt
	 * sind.
	 * 
	 * @param s Ship
	 */
	public void addShip(Ship ship) {
		for (int i = 0; i < ships.length; i++) {
			if (ships[i] == null) {
				ships[i] = ship;
				break;
			}
		}
	}

	/**
	 * Gibt alle Schiffe des Wettkampfs auf die Konsole aus
	 */
	public void printResult() {

		Arrays.sort(this.ships, new Comparator<Ship>() {

			public int compare(Ship s1, Ship s2) {

				if (s1 == null && s2 == null) {
					return 0;
				}
				if (s1 == null) {
					return 1;
				}
				if (s2 == null) {
					return -1;
				}

				return Integer.compare(s1.getTime(), s2.getTime());
			}
			// Alternative:
			// return s1.getTime() - s2.getTime();}
		});

		// Alternative:
		//Arrays.sort(this.ships, Comparator.nullsLast(Comparator.comparing(Ship::getTime)));

		System.out.println("Wettkampf: " + this.name);

		for (Ship s : this.ships) {
			if (s != null) {
				LocalTime time = LocalTime.ofSecondOfDay(s.getTime());
				System.out.println("Schiff Nr: " + s.getNr() + " Name: " + s.getName() + " Zeit: " 
						+ time.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
			}
		}
		
	}
}
```


