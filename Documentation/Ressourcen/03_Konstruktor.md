# Konstruktor

Der Konstruktor bietet die Möglichkeit, Objekte direkt mit Startwerten zu versehen und Initialisierungsarbeiten vorzunehmen. Er läuft bei Objekterstellung ab.



## Skript Konstruktor

Untenstehendes Skript erklärt das Strukturelement namens Konstruktor. 

[[M320_W02_2_Skript_Konstruktor.pdf]]



## Konstruktor implementieren

Sie können die Thematik gleich üben, indem Sie die Klasse Account um Konstruktoren erweitern. Bearbeiten Sie untenstehenden Auftrag dazu.

[[M320_W02_2_Konstruktor_implementieren.pdf]]


### Lösung:

[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_2/Konstruktoren)


### Musterlösung:

`AccountApplication.java`
```java
import java.util.Scanner;

public class AccountApplication {
	
	public static void main(String[] args) {
        System.out.println("Welcome to the account application");
        Account account = new Account(100);  // hier wird ein Objekt der Klasse `Account` erstellt
        double amount = 0;
        String command = "";
        
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Please enter the amount, 0 (zero) to terminate");
            amount = sc.nextDouble();
            if (amount != 0) {
                System.out.println("To deposit, press +, to withdraw press -");
                command = sc.next();
                if (command.equals("+")) {
                    account.deposit(amount);
                } else if (command.equals("-")) {
                    account.withdraw(amount);
                }
            }
        } while (amount != 0);
        
        System.out.println("Final balance: " + account.getBalance());
    }
}
```

`Account.java`
```java
public class Account {
	private double balance;
	
	public Account() {}
	
	public Account(double initialBalance) {
		this.balance = initialBalance;
	}

	public void deposit(double amount) {
		balance += amount;
	}

	public void withdraw(double amount) {
		balance -= amount;
	}

	public double getBalance() {
		return balance;
	}
}
```