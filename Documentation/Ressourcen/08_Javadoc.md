# Javadoc
Mit Javadoc lässt sich Sourcecode strukturiert dokumentieren, sodass die Entwicklungsumgebungen die Kommentare auch verarbeiten und beim Aufruf von Codeelementen anzeigen.



## Präsentation

[[M320_W04_1_Javadoc.pdf]]

## Tabelle

|Javadoc-Tag|Verwendungszweck|Beispiel|
|---|---|---|
|`@param`|Beschreibt einen Methodeparameter|`@param username Der Benutzername, der übergeben wird.`|
|`@return`|Gibt den Rückgabewert einer Methode an|`@return Gibt den berechneten Durchschnitt zurück.`|
|`@throws`|Beschreibt mögliche Ausnahmen (Exceptions)|`@throws IllegalArgumentException Wenn der Wert negativ ist.`|
|`@see`|Verlinkt auf andere Klassen oder Methoden|`@see Math#sqrt(double)`|
|`@author`|Gibt den Autor oder die Autoren des Codes an|`@author Max Mustermann`|
|`@version`|Gibt die Version des Codes an|`@version 1.0`|
|`@since`|Gibt an, seit welcher Version verfügbar|`@since 2.0`|
|`@deprecated`|Markiert ein Element als veraltet|`@deprecated Verwenden Sie stattdessen die neue Methode xyz()`|


## Aufgabe

Unten finden Sie das Regatta Projekt als Zip-Datei. Laden Sie die Datei herunter, entpacken Sie die Dateien in einen Ordner und importieren Sie das Projekt in Ihre Entwicklungsumgebung.  
In eclipse: File -> Import -> General -> Existing Projects into Workspace -> danach auf Next -> mit Browse den entpackten Ordner angeben und mit Finish bestätigen.  
In IntelliJ: File -> New -> Project from Existing Sources

Dokumentieren Sie die Klassen Start, Competition und Ship mit Javadoc. Verwenden Sie @author, @version, @param und @return, so dass Sie bei der Arbeit sinnvolle Hilfestellungen erhalten. Versuchen Sie auch @see einzusetzen.


[[W04-Javadoc.zip]]



## Lösung

Hier ist meine Lösung in meinem
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_4/Javadoc)



## Musterlösung

`Start.java`
```java
/**
 * Starterclass for testing
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Start {

	/**
	 * start method
	 * @param args
	 */
	public static void main(String[] args) {
		
		Competition c = new Competition("Rotsee Regatta");
		
		Ship ship1 = new Ship(1, "Alinghi");
		Ship ship2 = new Ship(2, "Red Baron");
		Ship ship3 = new Ship(3, "Blue Lagoon");
		
		c.addShip(ship1);
		c.addShip(ship2);
		c.addShip(ship3);
		
		c.start();
	
		c.printResult();
	}	
}
```

`Ship.java`
```java
/**
 * Represents a ship with its data
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Ship {

	private int nr;
	private String name;
	private int time;

	/**
	 * creates a ship object
	 * @param nr number
	 * @param name name
	 */
	public Ship(int nr, String name) {
		this.nr = nr;
		this.name = name;
	}

	
	/**
	 * get the number of the ship
	 * @return the nr
	 */
	public int getNr() {
		return nr;
	}


	/**
	 * set the number of the ship
	 * @param nr the nr to set
	 */
	public void setNr(int nr) {
		this.nr = nr;
	}



	/**
	 * get the name of the ship
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * set a name of the ship
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * calculates the race time for this ship
	 * @see #time
	 */
	public void race() {
		int min = 300;
		int max = 600;
		this.time = (int) (Math.random() * (max - min + 1)) + min;
	}
	
	/**
	 * get the race time of the ship
	 * @return time in seconds
	 */
	public int getTime() {
		return time;
	}
	
}
```

`Competition.java`
```java
/**
 * Class that simulates a regatta
 * 
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Competition {

	private String name;
	private Ship[] ships = new Ship[5];

	/**
	 * creates a competition object
	 * 
	 * @param name name of the competition
	 */
	public Competition(String name) {
		this.name = name;
	}

	/**
	 * starts a competition and let every ship do the race
	 * 
	 * @see Ship#race()
	 */
	public void start() {

		for (Ship s : this.ships) {
			if (s != null) {
				s.race();
			}
		}
	}

	/**
	 * adds a ship to the competition, if there are free places
	 * 
	 * @param ship the ship to add
	 */
	public void addShip(Ship ship) {
		for (int i = 0; i < ships.length; i++) {
			if (ships[i] == null) {
				ships[i] = ship;
				break;
			}
		}
	}

	/**
	 * prints status of all ships to console
	 */
	public void printResult() {
		System.out.println("Wettkampf: " + this.name);

		for (Ship s : this.ships) {
			if (s != null) {
				System.out.println("Schiff Nr: " + s.getNr() + " Name: " + s.getName() + " Zeit: " + s.getTime());
			}
		}
	}
}
```