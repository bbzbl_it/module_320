# Abstrakte Klassen


Wird eine Oberklasse abstrakt, lässt sich ein noch höheres Abstraktionsniveau erreichen. Gewisse Methoden sind dann in dieser Klasse z.B. nicht mehr implementiert, sondern nur noch definiert.


## Lernvideo

[Lernvideo Auf Youtube](https://www.youtubeeducation.com/watch?v=F2dsyn68N9o)

## Factsheet

Das wichtigste zum Thema abstrakte Klassen auf einer Seite:

[[M320_W06_1_Factsheet.pdf]]


## Arbeitsblatt

Arbeitsblatt zu abstrakten Klasse als Vertiefung.

[[M320_W06_2_Uebung_Abstrakte_Klassen.pdf]]

### Lösung

Hier ist meine Lösung in meinem
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_6/Abstrakte Klassen)


## Musterlösung

[Google Drive](https://docs.google.com/folderview?authuser=0&id=1LArSi1KnOtQPYuS0oRMF4dAHYMLE3UQq "„Drive Folder, src“ in neuem Fenster öffnen")


