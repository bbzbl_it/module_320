# ArrayList< T >

Sie kennen bisher normale Arrays, um mehrere Werte oder Objekte in einer einzigen Variable speichern zu können. Arrays müssen bei der Initialisierung immer direkt mit einer Grösse (Anzahl Elemente) initialisiert werden.

Um dieser Einschränkung entgegen zu wirken, gibt es in Java das sogenannte Collection Framework. Java definiert viele verschiedene Arten von Collections, eine davon ist die ArrayList.



## Einführung

Der wichtigste Unterschied von einem Array zu einer ArrayList ist die Tatsache, dass sich die ArrayList dynamisch vergrössern kann. Es handelt sich also um eine Struktur, wobei wir uns selber nicht mehr um die Anzahl Elemente die Platz haben, kümmern müssen.

Der Zugriff auf einzelne Elemente in der ArrayList erfolgt im Verlgeich zu anderen Listen-Arten sehr schnell in Java. Wir beschränken uns aber hier lediglich auf die ArrayList.


### Definition

```java
ArrayList<String> liste = new ArrayList<String>();
```

In den spitzen Klammern wird jeweils der Datentyp angegeben, wovon sich Elemente in der Liste befinden können.

In obiger Liste können also nur Strings gespeichert werden.

Eine ArrayList kann keine primitiven Datentypen wie int, long, double etc. verwalten, sondern nur Objekte. Möchte man trotzdem primitive Datentypen speichern, muss man sogenannte Wrapperklassen benutzen:

```java
ArrayList<Integer> liste = new ArrayList<Integer>();
```


### Liste durchlaufen

Sie können die Liste wie bei einem normalen Array mit einer for- oder foreach-Schlaufe durchlaufen.

```java
for(String element : liste) {

    System.out.println(element);

}
```


### Methoden

Nachdem die ArrayList wie oben gezeigt definiert wurde, können auf der Variable diverse Methoden zur Manipulation aufgerufen werden.

Hier eine Auswahl der wichtigsten Methoden:

|auf Element zugreifen|liste.get(1);|
|---|---|
|Element einfügen (ohne Angabe einer Position)|liste.add("Peter");|
|Element einfügen (mit Angabe der Position)|liste.add(0, "Peter);|
|Element ändern|liste.set(0, "Hans");|
|Element entfernen|liste.remove("Hans");  <br>               oder  <br> liste.remove(1); // Index|
|Grösse abfragen (=Anzahl eingefügte Elemente)|liste.size();|
|Prüfen, ob Objekt enthalten ist|liste.contains("Hans");|
|Index eines Objekts auslesen|liste.indexOf("Hans");|
|Liste leeren|liste.clear();|



## Präsentation

Hier finden Sie die gezeigte Präsentation während des Unterrichts:

[[M320_W03_1_W03_ArrayList.pdf]]



## Arbeitsblatt Regatta mit ArrayList

In der vorherigen Version von Regatta konnten nur eine begrenzte Anzahl Schiffe teilnehmen. Die Aufgabe soll nun auf eine ArrayList umgestellt werden, sodass beliebig viele Schiffe teilnehmen können.

[[M320_W03_2_Regatta_ArrayList.pdf]]


### Lösung:
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_3/ArrayList)