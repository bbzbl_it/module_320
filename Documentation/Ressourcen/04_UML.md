# UML

UML (Unified Modeling Language) definiert Diagramme, welche statische Strukturen von Programmen aber auch dynamischen Ablauf visualisieren kann. Wir beschränken uns an dieser Stelle auf das UML Klassendiagramm.

Ein Klassendiagramm ist ein Strukturdiagramm der [Unified Modeling Language](https://de.wikipedia.org/wiki/Unified_Modeling_Language) (UML) zur grafischen Darstellung (Modellierung) von [Klassen](https://de.wikipedia.org/wiki/Klasse_%28objektorientierte_Programmierung%29), [Schnittstellen](https://de.wikipedia.org/wiki/Schnittstelle_%28Objektorientierung%29) sowie deren Beziehungen.



## Präsentation

Hier die Präsentation zum Thema.

[[M320_W02_3_UML.pdf]]

### Lösung zu den Aufgaben in der Präsentation

Es gab zwei Aufgaben die in der Präsentation waren.
Folgend sind sie aufgelistet:

#### Einfaches UML

[[UML_praesentation_Loesung_1.png]]


#### UML mit Beziehungen

[[UML-mit-Bezihungen.png]]



## Klassendiagramm erstellen

In einem ersten Schritt haben Sie bereits ein Klassendiagramm aufgrund einer vorhandenen Klasse erstellt. Klassendiagramme können auch dazu genutzt werden, die Implementierung zu planen. Sie finden untenstehend eine Problembeschreibung. Aufgrund dieser designen Sie eine Klassen in Form eines Klassendiagramms.

[[M320_W02_3_UML_Klassendiagramm_erstellen.pdf]]


### Lösung:

[[UML_Loesung.png]]


### Musterlösung:

[[UML-Ship.png]]


### << use >> - Beziehung

"Benutzt" eine Klasse eine andere Klasse lediglich innerhalb einer Methode und speichert z.B. ein Objekt einer anderen Klasse NICHT als Instanzvariable, handelt es sich um keine der eingeführten Beziehungsarten Assoziation, Aggregation oder Komposition. In diesem Falle spricht man von einer << use >> Beziehung, welche fakultativ eingezeichnet werden kann. Bis jetzt haben Sie dies im Zusammenhang mit der main-Methode kennengelernt:

[[use.png]]


### Interfaces in UML

Ein Java-Interface wird in UML durch ein Rechteck mit abgerundeten Ecken dargestellt. Der Name des Interfaces wird innerhalb dieses Rechtecks geschrieben, oft kursiv. Es gibt keine weiteren Symbole oder Elemente, die speziell für die Darstellung von Java-Interfaces in UML erforderlich sind. Hier ist eine einfache Darstellung:

```
+-----------------------+ 
|   <<Interface>>       | 
|   MeineSchnittstelle  | 
+-----------------------+ 
|   +Methode1()         | 
|   +Methode2()         | 
|   +Methode3()         | 
+-----------------------+
```

In diesem Beispiel wird das Java-Interface "MeineSchnittstelle" dargestellt, wobei ``<<Interface>>`` die UML-Notation für ein Interface ist, und der Name des Interfaces darunter geschrieben ist.


#### Auftrag

Hierzu gab es leider keinen Auftrag, doch ich habe mich trotzdem daran versucht.

Ich will versuchen einen Teil des [[12_Interfaces#Arbeitsblatt Game|Game mit Interfaces]] als UML darzustellen. Zumindest den Teil mit dem Interface.

#### Lösung

Hier ist meine Lösung:

[[UML_Interface.png]]