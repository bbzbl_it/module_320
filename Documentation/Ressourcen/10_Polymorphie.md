
# Polymorphie


Polymorphie ermöglicht, spezifischere Objekte in Variablen eines verallgemeinerten Typs zu speichern. Durch Konzepte wie "Überschreiben" kann zur Laufzeit jedoch trotzdem Code des spezifischeren Typs ausgeführt werden.

  


## Präsentation

Hier die Präsentation zum Thema.

[[M320_W05_3_Praesentation_Polymorphie.pdf]]


## Arbeitsblatt

Hier das Arbeitsblatt zum Thema:

[[M320_W05_4_Uebung_Polymorphie.pdf]]

### Lösung

Hier ist meine Lösung in meinem
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_5/Polymorphie)



## Musterlösung

[Google Drive](https://docs.google.com/folderview?authuser=0&id=1KRKQXC-mv9w6nezkH7RV72Ra6C3w-c20 "„Drive Folder, src“ in neuem Fenster öffnen")

## Figuren Beispiel aus dem Unterricht

![](https://lh5.googleusercontent.com/FEgJJJ72aqke8pI_teBdMRvhFMBYy6oUBjvDce7hQf5ICZcDx9EG6dLvXYChjcRy24CI_XJ0YNcfP-28D_rLIB5bnmiGhFgAUSQZRlEcgfai839uHbRiV661cS1-P-c3tg=w1280)