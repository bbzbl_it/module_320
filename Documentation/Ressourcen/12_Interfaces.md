# Interfaces

Die abstrakteste Form einer abstrakten Klasse ist das Interface. Darin wird festgelegt, WAS eine Implementierung machen muss, aber nicht das WIE.


## Einführung

Folgendes Dokument soll Ihnen eine Einführung in Java Interfaces geben. Es ist beispielhaft aufgebaut, wobei diverse Möglichkeiten vorgestellt, diskutiert und bewertet werden.

Sämtlicher Sourcecode, welcher in diesem Dokument verwendet wird, ist auf dieser Seite unter der Rubrik 'Dokumente' als Zip-Datei zu finden. Sie können Ihn also in ihre Entwicklungsumgebung importieren.

Gehen Sie das Dokument in Ruhe durch und versuchen Sie sämtliche Erklärungen nachzuvollziehen. Danach beantworten Sie die Fragen gemäss Auftrag.

[[M320_W06_3_Skript_Interfaces.pdf]]

## Dokumente

Sourcecode des Skripts

[[W06-Interfaces-Skript.zip]]

Auftrag im Word Format

[[M320_W06_4_1_Interfaces_Fragen.docx]]


## Arbeitsblatt

Beantworten Sie die Fragen aus folgendem Arbeitsblatt gemäss der Methode think - pair - share:

Das Dokument ist auch als Word-Dokument unter 'Dokumente' abrufbar.

[[M320_W06_4_1_Interfaces_Fragen.pdf]]

Zusätzliche Hinweise zu Interfaces finden Sie auf dieser Seite: [http://www.kstbb.de/informatik/oo/21/index.html](http://www.kstbb.de/informatik/oo/21/index.html)

### Lösung

Dies ist meine Lösung:
[[M320_W06_4_1_Interfaces_Fragen_geloest.docx]]

## Arbeitsblatt Game

Folgender Auftrag dient der Übung der Thematik Interfaces. Die Aufgabe baut auf einem bereits bekannten Beispiel auf.

[[M320_W06_5_Game.pdf]]

### Lösung

Hier ist meine Lösung in meinem
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_6/Game)


## Musterlösung

[Google Drive](https://docs.google.com/folderview?authuser=0&id=1L5YSUnI_YDIuTfSVUxZmKD--IpLoPUsh "„Drive Folder, src“ in neuem Fenster öffnen")