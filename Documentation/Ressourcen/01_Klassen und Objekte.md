# Klassen und Objekte

Das Implementieren von Klassen und Arbeiten mit Objekten stellt die Basis in der Objektorientierten Programmierung dar.


## Repetition Vorwissen

Anhand des Wissens und Könnens aus dem Vormodul 319 entwickeln Sie ein Programm, welches einen einfachen Dialog für ein Bankkonto realisiert.


[[M320_W01_1_Repetitionsaufgabe_AccountApplication.pdf]]


### Lösung:


[GitLab Repository](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_1/AccountApplication)


### Musterlösung:


`AccountApplication.java`
```java
import java.util.Scanner;

public class AccountApplication {
	
	public static void main(String[] args) {
		System.out.println("Welcome to the account application");
		double balance = 0;
		double amount = 0;
		String command = "";
		
		do {
			Scanner sc = new Scanner(System.in);
			System.out.println("Please enter the amount, 0 (zero) to terminate");
			amount = sc.nextDouble();
			if (amount != 0) {
				System.out.println("To deposit, press +, to withdraw press -");
				command = sc.next();
				if (command.equals("+")) {
					balance = deposit(balance, amount);
				} else if (command.equals("-")) {
					balance = withdraw(balance, amount);
				}
			}
		} while (amount != 0);
		
		System.out.println("Final balance: " + balance);
	}

	public static double deposit(double balance, double amount) {
		return balance + amount;
	}

	public static double withdraw(double balance, double amount) {
		return balance - amount;
	}
}

```



## Analyse AccountApplication

Sie haben die erste Aufgabe basierend auf Ihrem Kenntnisstand aus Modul 319 gelöst. Wir wollen die vorliegende Lösung nun in einem ersten Schritt analysieren:


[[M320_W01_2_Analyse_AccountApplication.pdf]]


### Musterlösung:

[[M320_W01_2_Analyse_AccountApplication_Loesung.pdf]]



## Fachklassen

Wir haben nun gesehen, dass das Programm zu viele Aufgaben bearbeitet. Die Arbeit wollen wir nun aufteilen in zwei Klassen. Eine Klasse, die nur im Hintergrund arbeitet und nicht für die Interaktion mit dem Benutzer zuständig ist, nennt man Fachklasse.


### Skript Fachklassen

[[M320_W01_3_Skript_Fachklassen.pdf]]



## Refactoring AccountApplication

REFACTORING

Bezeichnung im Programmieren, dass man den vorhandenen Code neu strukturiert, ohne neue Funktionalität hinzuzufügen. Refactoring dient dazu, dass die Applikation/Software auf lange Zeit besser wartbar und erweiterbar ist.

[[M320_W01_3_Refactoring_AccountApplication.pdf]]


### Lösung:

[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_1/AccountApplicationRefactored)


### Musterlösung:

`AccountApplication.java`
```java
import java.util.Scanner;

public class AccountApplication {
	
	public static void main(String[] args) {
        System.out.println("Welcome to the account application");
        Account account = new Account();  // hier wird ein Objekt der Klasse `Account` erstellt
        double amount = 0;
        String command = "";
        
        do {
            Scanner sc = new Scanner(System.in);
            System.out.println("Please enter the amount, 0 (zero) to terminate");
            amount = sc.nextDouble();
            if (amount != 0) {
                System.out.println("To deposit, press +, to withdraw press -");
                command = sc.next();
                if (command.equals("+")) {
                    account.deposit(amount);
                } else if (command.equals("-")) {
                    account.withdraw(amount);
                }
            }
        } while (amount != 0);
        
        System.out.println("Final balance: " + account.getBalance());
    }
}
```

`Account.java`
```java
public class Account {
	private double balance;

	public void deposit(double amount) {
		balance += amount;
	}

	public void withdraw(double amount) {
		balance -= amount;
	}

	public double getBalance() {
		return balance;
	}
}
```


