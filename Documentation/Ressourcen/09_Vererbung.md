# Vererbung

Mit Vererbung kann ein höherer Abstraktionsgrad erreicht werden. Gemeinsame Attribute und Methoden werden in seiner Oberklasse vereint, Unterklassen erweitern die Funktionalität.

## Reale Objekte

In der Softwareentwicklung geht es meist darum, etwas aus der realen Welt in Form von Software abzubilden. Dies sind z.B. Geschäftsprozesse oder je nach Branche spezifische Produkte. Sie haben bereits im Vormodul gelernt, wie Sie von etwas "Realem" sogenannte Klassen bilden können. Dargestellt haben wir diese Klassen mit der standardisierten Darstellung nach UML (Unified Modeling Language).

Daran knüpfen wir mit den folgenden Übungen an:

[[A_Vererbung_Einführung_Auto_Variablen.pdf]]
[[A_Vererbung_Einführung_AutoMethoden.pdf]]
[[A_Vererbung_Einführung_Elektroauto_Methoden.pdf]]
[[A_Vererbung_Einführung_Elektroauto_Variablen.pdf]]
[[A_Vererbung_Einführung_LKW_Methoden.pdf]]
[[A_Vererbung_Einführung_LKW_Variablen.pdf]]


## Vererbung implementieren

Wie kann Vererbung konkret mit einer Programmiersprache implementiert werden. Folgendes Lernvideo erklärt die Umsetzung mit Java:

### Lernvideo

(Das Video stammt aus Modul 226B, worin Vererbung ebenfalls thematisiert wird).

[Das Youtube Video](https://www.youtubeeducation.com/watch?v=_yT-_Tm0XMg)

Weitere Informationen zum Thema Vererbung finden Sie z.B. hier: [http://www.kstbb.de/informatik/oo/10/index.html](http://www.kstbb.de/informatik/oo/10/index.html) (Kaufmännische Schule Tauberbischofsheim)

### Übung Car

Folgende Übung dient zur Vertiefung des Konzeptes der Vererbung.

[[M320_W05_2_Uebung_Vererbung.pdf]]

### Lösung

Hier ist meine Lösung in meinem
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_5/Vererbung)



### Musterlösung

`test/TestCars.java`
```java
package test;

import cars.CrashedCar;
import cars.UsedCar;

public class TestCars {

	public static void main(String[] args) {
		// anstelle CrashedCar.DMG_LVL_LOW auch 0 ok
		CrashedCar cc = new CrashedCar("BMW M3", 10000, CrashedCar.DMG_LVL_LOW);
		print("low", 10000*0.9, cc.getPrice());
		
		cc.setDamageLevel(CrashedCar.DMG_LVL_MEDIUM);
		print("medium", 10000*0.5, cc.getPrice());

		cc.setDamageLevel(CrashedCar.DMG_LVL_TOTAL);
		print("total", 0, cc.getPrice());

		double price = 80000;
		UsedCar uc = new UsedCar("Audi A6", price, 0);
		print("u0", price, uc.getPrice());

		uc.setMileage(10000);
		print("u10000", 76000, uc.getPrice());

		uc.setMileage(200000);
		print("u200000", 0, uc.getPrice());

		uc.setMileage(210000);
		print("u210000", 0, uc.getPrice());

	}

	private static void print(String id, double expected, double effective) {
		System.out.println(id + " expected " + expected + ", effective: "+ effective);
	}
}
```

`cars/Car.java`
```java
package cars;

public class Car {
	
	private String model;
	private double price;
	
	public Car(String model, double price) {
		this.model = model;
		this.price = price;
	}

	protected double getCarPrice() {
		return price;
	}
}
```

`cars/CrashedCar.java`
```java
package cars;

public class CrashedCar extends Car {

	//an enum would be nicer
	public static final int DMG_LVL_LOW = 0;
	public static final int DMG_LVL_MEDIUM = 1;
	public static final int DMG_LVL_TOTAL = 2;
	
	private int damageLevel;
	
	public CrashedCar(String model, double price, int damageLevel) {
		super(model, price);
		this.damageLevel = damageLevel;
	}

	public double getPrice() {
		double pr = this.getCarPrice();
		switch (this.damageLevel) {
		case DMG_LVL_LOW: //case 0:
			pr *= 0.9; // pr = pr * 0.9
			break;
		case DMG_LVL_MEDIUM: // case 1:
			pr *= 0.5;
			break;
		case DMG_LVL_TOTAL: // case 2:
			pr = 0;
			break;

		}
		return pr;
	}
	
	/**
	 * @return the damageLevel
	 */
	public int getDamageLevel() {
		return damageLevel;
	}

	/**
	 * @param damageLevel the damageLevel to set
	 */
	public void setDamageLevel(int damageLevel) {
		this.damageLevel = damageLevel;
	}

}
```

`cars/UsedCar.java`
```java
package cars;

public class UsedCar extends Car {

	private int mileage;
	
	public UsedCar(String model, double price, int mileage) {
		super(model, price);
		this.mileage = mileage;
	}
	
	public double getPrice() {
		
		double priceOneKm = (this.getCarPrice() / 100) * 0.0005;
		double newPrice = this.getCarPrice() - (priceOneKm * this.mileage);
		if(newPrice < 0) {
			newPrice = 0;
		} 
		return newPrice;
	}
	
	/**
	 * @return the mileage
	 */
	public int getMileage() {
		return mileage;
	}

	/**
	 * @param mileage the mileage to set
	 */
	public void setMileage(int mileage) {
		this.mileage = mileage;
	}
}
```
