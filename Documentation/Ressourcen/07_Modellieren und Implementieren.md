# Modellieren und Implementieren

In der Realität werden Kundenanforderungen in Softwareprogrammen abgebildet. Dazu müssen aus Dokumentationen, Beschreibungen, Interviews etc.  die relevanten Inhalte extrahiert und in einer Klassenstruktur abgebildet werden. 

Um das Programm zu planen kann ein UML Klassendiagramm helfen, die Struktur zu finden. Danach wird das Programm umgesetzt.


In diesem Abschnitt finden Sie zwei Aufgabenstellungen. Beide beschreiben die Anforderungen an ein Programm. Lesen Sie beide Beschreibungen durch und entscheiden Sie sich für eine Aufgabe, welche Sie dann auch umsetzen.



## Invoice

In dieser Aufgabenstellung geht es darum, eine Rechnung, welche mehrere Rechnungspositionen aufnehmen kann, in einem Programm abzubilden.

[[M320_W03_3_Invoice.pdf]]


### Alternatives Vorgehen

Das Vorgehen ist im Auftrag beschrieben. 

Falls es Ihnen schwer fällt, eine Klassenstruktur für das Problem zu definieren, hilft Ihnen die Vorgabe. Überlegen Sie sich jedoch, ob Sie es nicht zuerst selbst mit einer eigenen Struktur versuchen wollen. Daraus könnte ein interessanter Lerneffekt resultieren.

[[M320_W03_3_Invoice_mit_Vorgabe.pdf]]  



## Game

In dieser Aufgabenstellung geht es darum, im Rahmen eines Spiels Charaktere gegeneinander antreten zu lassen. Der Gewinner ist der letzte Überlebende!

Diese Aufgabe ist etwas anspruchsvoller wie Invoice.

[[M320_W03_4_Game.pdf]]


### Alternatives Vorgehen

Das Vorgehen ist im Auftrag beschrieben. 

Falls es Ihnen schwer fällt, eine Klassenstruktur für das Problem zu definieren, hilft Ihnen die Vorgabe. Überlegen Sie sich jedoch, ob Sie es nicht zuerst selbst mit einer eigenen Struktur versuchen wollen. Daraus könnte ein interessanter Lerneffekt resultieren.

[[M320_W03_4_Game_mit_Vorgabe.pdf]]  


## Meine Lösung

Ich habe Mich dafür entschieden, das Invoice Programm zu erstellen. Dies habe ich zusammen mit meinem Partner gelöst.

- Der Code dazu ist auf [GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_3/InvoiceProgram) 
- Zudem gab es noch ein [[UML-mit-Bezihungen.png|UML zur Aufgabe]] 



## Musterlösungen

Konsultieren Sie die Musterlösungen erst am Ende.


### Invoice

`Invoice.java`
```java
import java.util.ArrayList;

public class Invoice {

	private ArrayList<Item> items;
	
	public Invoice() {
		//kann auch direkt bei der Variable initialisiert werden
		this.items = new ArrayList<Item>();
	}
	
	public void addItem(Item item) {
		this.items.add(item);
	}

	public double getInvoiceAmountExcl() {
		double amount = 0;
		
		for(Item p : this.items) {
			amount += p.getPrice();
		}
		
		return amount;
	}
	
	public void printInvoice() {
		System.out.println("**********Rechnung**********");
		for(Item p : this.items) {
			System.out.println(p.getText() + ": " + p.getPrice());
		}
		System.out.println("TOTAL exkl. Mwst: " + this.getInvoiceAmountExcl());
	}
	
}
```

`Item.java`
```java
public class Item {

	private String text;
	private double price;
	
	public Item(String text, double price) {
		this.text = text;
		this.price = price;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
```

`Tester.java`
```java
public class Tester {

	public static void main(String[] args) {
		
		Invoice invoice = new Invoice();
		
		Item item1 = new Item("Haarschaum", 4.90);
		Item item2 = new Item("Brot", 2.90);
		Item item3 = new Item("Cola 1l", 1.95);
		Item item4 = new Item("Snickers", 2.60);

		invoice.addItem(item1);
		invoice.addItem(item2);
		invoice.addItem(item3);
		invoice.addItem(item4);
		
		invoice.printInvoice();
	}

}
```


### Invoice Erweiterung

`Invouce.java`
```java
import java.util.ArrayList;

public class Invoice {

	private ArrayList<Item> items;
	private double vatRate = 7.7;
	
	public Invoice() {
		//kann auch direkt bei der Variable initialisiert werden
		this.items = new ArrayList<Item>();
	}
	
	public void addItem(Item item) {
		this.items.add(item);
	}

	public double getInvoiceAmountExcl() {
		double amount = 0;
		
		for(Item p : this.items) {
			amount += p.getPrice();
		}
		
		return amount;
	}

	public void setVatRate(double vatRate) {
		if(this.vatRate >= 0) {
			this.vatRate = vatRate;
		}
	}
	
	public double getInvoiceAmountIncl() {
		double amount = 0;
		
		amount = this.getInvoiceAmountExcl() * (1 + this.vatRate/100);
		//oder je Item
		/*for(Item i : this.positionen) {
			amount += i.getPrice() + (i.getPrice() / 100 * this.vatRate);
		}*/
		
		return amount;
	}
	
	public void printInvoice() {
		System.out.println("**********Rechnung**********");
		for(Item p : this.items) {
			System.out.println(p.getText() + ": " + p.getPrice());
		}
		System.out.println("TOTAL exkl. Mwst: " + this.getInvoiceAmountExcl());
		System.out.println("TOTAL inkl. Mwst: " + this.getInvoiceAmountIncl());
	}
	
}
```

`Item.java`
```java
public class Item {

	private String text;
	private double price;
	
	public Item(String text, double price) {
		this.text = text;
		this.price = price;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
}
```

`Tester.java`
```java
public class Tester {

	public static void main(String[] args) {
		
		Invoice invoice = new Invoice();
		
		Item item1 = new Item("Haarschaum", 4.90);
		Item item2 = new Item("Brot", 2.90);
		Item item3 = new Item("Cola 1l", 1.95);
		Item item4 = new Item("Snickers", 2.60);

		invoice.addItem(item1);
		invoice.addItem(item2);
		invoice.addItem(item3);
		invoice.addItem(item4);
		
		double invAmount = invoice.getInvoiceAmountExcl();
		System.out.println("Rechnungsbetrag exkl. Mwst: " + invAmount);
		
		invoice.setVatRate(2.5);
		double invAmountIncl = invoice.getInvoiceAmountIncl();
		System.out.println("Rechnungsbetrag inkl. Mwst: " + invAmountIncl);
		
		invoice.printInvoice();
	}

}
```


### Game

`Character.java`
```java
public class Character {

	private String name;
	private int hp; // health
	private int ap; // attack
	
	public Character(String name, int hp, int ap) {
		this.name = name;
		this.hp = hp;
		this.ap = ap;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHp() {
		return hp;
	}

	private void setHp(int hp) {
		if(hp < 0) {
			this.hp = 0;
		} else {
			this.hp = hp;
		}
	}

	public int getAp() {
		return ap;
	}

	public void setAp(int ap) {
		this.ap = ap;
	}
	
	public void attack(Character enemy) {
		int hit = (int) (this.ap * Math.random());
		enemy.setHp(enemy.getHp() - hit);
		
		System.out.println(this.name + " hit " + enemy.name + " with " + hit);
		this.log(this);
		this.log(enemy);
	}
	
	private void log(Character character) {
		System.out.println(character.getName() +": " + "hp=" + character.getHp());
	}
}
```

`Fight.java`
```java
public class Fight {
	
	private Character player1 = null;
	private Character player2 = null;
	private boolean played = false;
	
	public Fight(Character player1, Character player2) {
		this.player1 = player1;
		this.player2 = player2;
	}
	
	public void fight() {
		this.played = false;
		while(player1.getHp() > 0 && player2.getHp() > 0) {
			
			this.player1.attack(this.player2);
			this.newRound();
			System.out.println("---");
		}
		played = true;
	}
	
	private void newRound() {
		Character tmp = this.player1;
		this.player1 = player2;
		this.player2 = tmp;
	}

	public Character getWinner() {
		if(played) {
			if(this.player1.getHp() > 0) {
				return player1;
			} else {
				return player2;
			}
		}
		return null;
	}
}
```

`Tester.java`
```java
public class Tester {

	public static void main(String[] args) {

		Character char1 = new Character("Zenturon", 100, 20);
		Character char2 = new Character("Valarian", 100, 20);

		Fight game = new Fight(char1, char2);
		game.fight();

		Character winner = game.getWinner();
		if (winner != null) {
			System.out.println(winner.getName() + " hat den Kampf gewonnen.");
		}
	}

}
```


### Game Erweiterung

`Character.java`
```java
public class Character {

	private String name;
	private int hp; // health
	private int ap; // attack
	private int maxHp;
	
	public Character(String name, int hp, int ap) {
		this.name = name;
		this.hp = hp;
		this.maxHp = hp;
		this.ap = ap;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHp() {
		return hp;
	}

	private void setHp(int hp) {
		if(hp < 0) {
			this.hp = 0;
		} else {
			this.hp = hp;
		}
	}

	public int getAp() {
		return ap;
	}

	public void setAp(int ap) {
		this.ap = ap;
	}
	
	public void heal() {
		this.hp = this.maxHp;
	}
	public void attack(Character enemy) {
		int hit = (int) (this.ap * Math.random());
		enemy.setHp(enemy.getHp() - hit);
		
		System.out.println(this.name + " hit " + enemy.name + " with " + hit);
		this.log(this);
		this.log(enemy);
	}
	
	private void log(Character character) {
		System.out.println(character.getName() +": " + "hp=" + character.getHp());
	}
}
```

`Fight.java`
```java
public class Fight {
	
	private Character player1 = null;
	private Character player2 = null;
	private boolean played = false;
	
	public Fight(Character player1, Character player2) {
		this.player1 = player1;
		this.player2 = player2;
	}
	
	public void fight() {
		this.played = false;
		while(player1.getHp() > 0 && player2.getHp() > 0) {
			
			this.player1.attack(this.player2);
			this.newRound();
			System.out.println("---");
		}
		played = true;
	}

	public Character getWinner() {
		if(played) {
			if(this.player1.getHp() > 0) {
				return player1;
			} else {
				return player2;
			}
		}
		return null;
	}
	
	public Character getLooser() {
		if(played) {
			if(this.player1.getHp() <= 0) {
				return player1;
			} else {
				return player2;
			}
		}
		return null;
	}
	
	private void newRound() {
		Character tmp = this.player1;
		this.player1 = player2;
		this.player2 = tmp;
	}
}
```

`Game.java`
```java
import java.util.ArrayList;
import java.util.Collections;

public class Game {

	private ArrayList<Character> players = new ArrayList<Character>();

	public void addCharacter(Character character) {
		this.players.add(character);
	}

	public Character start() {

		while (this.players.size() > 1) {
			System.out.println("---Neue Begegnung---");
			Collections.shuffle(players);
			
			Character player1 = this.players.get(0);
			Character player2 = this.players.get(1);
			Fight f = new Fight(player1, player2);
			f.fight();
			f.getWinner().heal();
			Character looser = f.getLooser();
			System.out.println(looser.getName() + " hat verloren und scheidet aus.");
			this.players.remove(f.getLooser());
		}
		if (this.players.size() > 0) {
			return this.players.get(0);
		} else {
			return null;
		}
	}
	
	public void printPlayers() {
		for(Character c : this.players) {
			System.out.println(c.getName() + " HP: " + c.getHp() + " AP: " + c.getAp());
		}
	}
}
```

`Tester.java`
```java
public class Tester {

	public static void main(String[] args) {

		Character char1 = new Character("Zenturon", 100, 20);
		Character char2 = new Character("Valarian", 100, 20);
		Character char3 = new Character("Backpipe", 100, 20);
		Character char4 = new Character("MrFlute", 100, 20);
		Character char5 = new Character("Hancock", 100, 20);
		
		Game game = new Game();
		game.addCharacter(char1);
		game.addCharacter(char2);
		game.addCharacter(char3);
		game.addCharacter(char4);
		game.addCharacter(char5);
		
		System.out.println("Spiel wird mit folgenden Charakteren gestartet:");
		game.printPlayers();

		Character winner = game.start();
		System.out.println(winner.getName() + " hat alle besiegt.");
		
	}

}
```
