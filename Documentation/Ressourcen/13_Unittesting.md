# Unittesting

Ein Softwaretest prüft und bewertet Software auf Erfüllung der für ihren Einsatz definierten Anforderungen und misst ihre Qualität. Die gewonnenen Erkenntnisse werden zur Erkennung und Behebung von Softwarefehlern genutzt. Tests während der Softwareentwicklung dienen dazu, die Software möglichst fehlerfrei in Betrieb zu nehmen. Mit der Verbreitung von [agilen Softwareentwicklungsmethoden](https://de.wikipedia.org/wiki/Agile_Softwareentwicklung) und insbesondere [testgetriebener Entwicklung](https://de.wikipedia.org/wiki/Testgetriebene_Entwicklung) ist es üblich geworden, Modultests möglichst automatisiert auszuführen. Dazu werden üblicherweise mit Hilfe von Test Frameworks wie beispielsweise [JUnit](https://de.wikipedia.org/wiki/JUnit) Testprogramme geschrieben. Über die Test Frameworks werden die einzelnen Testklassen aufgerufen und deren Komponententests ausgeführt. Die meisten Test Frameworks geben eine grafische Zusammenfassung der Testergebnisse aus.  
(Wikipedia)

## Präsentation

Hier ein Auszug aus der Präsentation des Unterrichts

[[M320_W08_1_Praesentation_Unittesting_1.pdf]]

## Erste JUnit Übung

In einer ersten Übung lernen Sie, wie Sie basierend auf einer Fachklasse eine Testklasse generieren können, um darin dann den Testcode zu schreiben.

[[M320_W08_2_Arbeitsblatt_JUnit5.pdf]]

### Lösung

Hier ist meine Lösung in meinem
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_8/Unittest/Calculator)

## Sinnvolle Tests

Vermutlich haben Sie obige Klasse Calculator mit irgendwelchen zufälligen Werten getestet. Wie man vorgehen könnte, um hier sinnvolle Werte zu wählen  und die Tests so zu schreiben, dass viel vom produktiven Code auch durch Tests abgedeckt ist, erfahren Sie in folgenden drei Folien.

[[M320_W08_1_Praesentation_Unittesting_2.pdf]]

## Auftrag Account

Sie erhalten den Auftrag, untenstehende Klasse Account möglichst vollständig zu testen. 

Account:
- Die Klasse simuliert ein Konto, womit man einen Kontostand (balance) verwalten kann. Dem Konstruktor kann ein Start-Kontostand übergeben werden. 
- Über die Methode getBalance() lässt sich der Kontostand abfragen. 
- Die Methode withdraw(...) ermöglicht das Abheben eines definierten Betrages (Parameter) vom Kontostand. 
- Die Methode deposit(...) ermöglicht das Einzahlen eines definierten Betrages (Parameter) auf das Konto.

Auftrag
- Erstellen Sie ein neues Projekt und kopieren Sie die zu testende Klasse in den Sourceordner
- Erstellen Sie eine neue Testklasse, analog zu der Einführungsübung  mit dem Wizard der Entwicklungsumgebung
- Initialisieren Sie ein Account-Objekt mit sinnvollem Start-Kontostand im @BeforeEach Konstrukt
- Testen Sie alle Methoden der Klasse
    - Bedingung beim Abheben (withdraw): Der Kontostand darf nicht überzogen werden
- Überlegen Sie sich, inwiefern das Objekt mit ungünstigen Werten initialisiert werden könnte und inwiefern Sie das im Test abdecken könnten

Files:
[[Account.java]]

### Lösung

Hier ist meine Lösung in meinem
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_8/Unittest/Account)

### Musterlösung Auftrag Account

Files:
[[AccountTest.java]]
