# Repetition

Auch im Hinblick auf die Prüfung am Ende des Moduls wurden folgende Repetitionsaufgaben zusammengestellt. Sie können Aufgaben nach diesem Schema erwarten.
- Programmieraufgabe "Mehrere Fachklassen"
- UML Klassendiagramm aufgrund einer Situation
- Programmieraufgabe Abstraktion
- Programmieraufgabe Unittesting

Infos zu LB2
- Dauer: 100 Minuten
- Umgebung: Clients der Schule in der Prüfungsumgebung
- Hilfsmittel: Ihr eigenes ePortfolio. Entweder auf dem eigenen Laptop im Flugmodus oder online am Prüfungsclient.
- 4 Aufgaben in oben erwähnten Themenbereichen
- Sie erhalten ein Vorlageprojekt, worin die Aufgaben zu lösen sind

## 1. Programmieraufgabe zu "Mehrere Fachklassen"

[[M320_W08_3_Repetition_MehrereFachklassen.pdf]]

## 2. UML Klassendiagramm

[[M320_W08_3_Repetition_UML.pdf]]

## 3. Programmieraufgabe zu Abstraktion

[[M320_W08_3_Repetition_Abstraktion.pdf]]

## 4. Programmieraufgabe zu Unittesting

[[M320_W08_3_Repetition_JUnit.pdf]]

Files:
[[Auto.java]]

## Lösung

Hier sind meine Lösungen in meinem
[GitLab Repo](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Repetition)

## Musterlösungen

### Aufgabe Mehrere Fachklassen - Recyhof

[Google Drive](https://docs.google.com/folderview?authuser=0&id=1WWT13ZG4csQXGiSRLVoJToPbBG3tmTIf "„Drive Folder, src“ in neuem Fenster öffnen")

### Aufgabe UML

[[M320_W08_3_Repetition_UML_ML.png]]

### Aufgabe Abstraktion - Bibliothek

[Google Drive](https://docs.google.com/folderview?authuser=0&id=1WjQQXNewLSCHV_qGuxeGy20NYUbbqdvS "„Drive Folder, src“ in neuem Fenster öffnen")

### Aufgabe JUnit

[Google Drive](https://docs.google.com/folderview?authuser=0&id=1WcqCZ1ii9fiZDp9tJT4MvPnI5S7-Omgj "„Drive Folder, src“ in neuem Fenster öffnen")

