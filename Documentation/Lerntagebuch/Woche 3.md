# Woche 3 // Lerntagebuch 30 08 2023

Lektionen:

- 08:05 - 11:35


### Was habe ich gemacht? 
Zuerst haben wir die Hausaufgaben zum Regatta Program aus der Letzten Lektion überprüft und besprochen. Anschliessend haben wir uns mit dem Thema ArrayList auseinandergesetzt und gelernt, wie man diese in Java verwendet.

Nach der theoretischen Einführung in ArrayList haben wir die Regatta Program umgestaltet, um ArrayLists anstelle von Arrays zu verwenden. Dies erforderte einige Änderungen im Code, um die neuen Konzepte zu integrieren.

Im Anschluss haben wir entweder an einem Invoice Program oder einem Game gearbeitet, je nachdem, welche Aufgabe wir gewählt haben. Ich habe das Invoice Program gewählt. 


### Was habe ich gelernt? 
Ich habe gelernt, wie man ArrayLists in Java verwendet und wie sie in der Programmierung nützlich sein können. 


### Was ist mir gut gelungen? 
Mir ist die Umstellung des Regatta Programs auf ArrayLists gut gelungen. Die Anwendung davon hat den Code effizienter und flexibler gemacht. Auch das Erstellen des Invoice Program ist mir relativ gut gelungen.


### Wie bin ich vorgegangen, dass es mir gelungen ist? 
Ich habe ich den bestehenden Code des Regatta Programs überarbeitet, um ArrayLists zu implementieren. Bei der Erstellung des Invoice Program oder Game habe ich die zuvor gelernten OOP-Konzepte angewendet.


### Womit hatte ich Schwierigkeiten? 
Ich hatte einige Schwierigkeiten beim Entwickeln der Hausaufgaben. 


### Wie habe ich auf die Schwierigkeiten reagiert? 
Ich konnte diese Schwierigkeiten schnell lösen, indem ich mir Hilfe von einem meiner Teamkollegen gehohlt habe


### Folgende Arbeiten habe ich ausgeführt: 
- Lerntagebuch Fortführen
- Hausaufgaben [[05_Mehrere Fachklassen#Erweiterung um eine neue Fachklasse|Regatta Program]] ansehen
- [[06_ArrayList#Einführung|AttayList]] ansehen
- [[06_ArrayList#Arbeitsblatt Regatta mit ArrayList|Regatta Program]] nach Auftrag umbauen
- [[07_Modellieren und Implementieren#Invoice|Invoice Program]] oder [[07_Modellieren und Implementieren#Game|Game]] erstellen
- Erstelltes Program mit Musterlösung vergleichen
- Lerntagebuch Fortführen



## Reflexion:


### Was ist mir heute klar geworden? 
Ich habe gelernt das wenn man einen Array mit unbestimmt vielen Items braucht, man eine  ArrayLists als Datenstruktur benutzen kann. Außerdem habe ich gelernt, wie man die verschiedenen OOP-Konzepte in Programmen anwendet.


### Was hat mich emotional angesprochen, bewegt, verunsichert, etc.? 
An diesem Tag gab es keine besonderen Ereignisse oder Herausforderungen.


### Was nehme ich in den Berufsalltag mit? 
Ich nehme mit, wie wichtig es ist, die richtige Datenstruktur für die jeweilige Aufgabe auszuwählen. Außerdem werde ich öfters Hilfe von meinen Kollegen holen, wenn ich Fragen in der Programmierung habe.


### Welche Fragen sind heute aufgetaucht?
Heute sind keine Fragen aufgetaucht

