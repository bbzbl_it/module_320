# Woche 1 // Lerntagebuch 16 08 2023

Lektionen:

- 08:05 - 11:35


### Folgende Arbeiten habe ich ausgeführt: 
- Eine [[01_Klassen und Objekte.md#Repetition Vorwissen|AccountApplication]] nach bereits bestehendem Wissen programmieren
- Eine Einführung in OOP durch [[01_Klassen und Objekte#Fachklassen|Fachklassen]] 
- Die [[01_Klassen und Objekte#Refactoring AccountApplication|AccountApplication Refactoring]] erstellen und das gewonnene Wissen von OOP implementieren


### TODO: 
- Installieren Eclipse 
- Programmieren: Account Application 
- Lernen OOP / Fachklassen 
- OOP in AccountApplication anwenden



## Reflexion:


### Was ist mir heute klar geworden? 

Das OOP ist ein wichtiges Konzept der Programmierung das einem viele unterschiedliche Erleichterungen ermöglicht


### Was hat mich emotional angesprochen, bewegt, verunsichert, etc.? 

Ich hatte etwas Probleme beim einrichten eines IDEs und habe deshalb viel Zeit verloren, da ich einen neuen Laptop habe und dieser noch nicht Konfiguriert war.


### Was nehme ich in den Berufsalltag mit? 

Das Java viele verschiedene Anwendungszweche hat auch für Web, etc.


### Welche Fragen sind heute aufgetaucht?

Im Moment keine
