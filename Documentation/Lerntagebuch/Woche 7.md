# Woche 7 // Lerntagebuch 27 09 2023

Lektionen:

- 08:05 - 11:35


### Was habe ich gemacht?

Heute habe ich eine Erweiterung zum Thema "Interfaces" erhalten. Diese Erweiterung half mir, mein Verständnis für Interfaces zu vertiefen und ihre vielfältigen Anwendungsmöglichkeiten besser zu erfassen.

Anschließend habe ich die Konzepte von Interfaces in einem Game implementiert. Dies war eine spannende Aufgabe, da sie mir die Gelegenheit bot, die praktische Anwendung von Interfaces in einer realen Programmierumgebung zu erleben.

Nachdem ich die Aufgaben zu Interfaces erfolgreich abgeschlossen hatte, widmete ich mich dem ePortfolio. Hier arbeitete ich an der Erweiterung und Aktualisierung meines elektronischen Portfolios, um meine Lernerfahrungen und Fortschritte festzuhalten.

### Was habe ich gelernt?

Die Erweiterung des Themas "Interfaces" half mir, weitere Details und Anwendungsfälle zu verstehen. Die Implementierung von Interfaces in einem Game ermöglichte es mir, die Flexibilität und die Vorteile dieser Konzepte in der Praxis zu erkennen.

### Was ist mir gut gelungen?

Die Implementierung von Interfaces in einem realen Game war eine herausfordernde, aber lohnende Aufgabe. Ich konnte die Schnittstellen effektiv nutzen, um verschiedene Spielobjekte zu steuern und sie miteinander zu verknüpfen.

### Wie bin ich vorgegangen, dass es mir gelungen ist?

Ich bin systematisch vorgegangen, indem ich zuerst das theoretische Wissen erweitert und vertieft habe. Dann habe ich die Konzepte praktisch in einem Game umgesetzt, wobei ich die klaren Schnittstellen und die Anwendung von Verträgen zwischen Klassen beachtet habe.

### Womit hatte ich Schwierigkeiten?

Während der Implementierung von Interfaces in einem Game gab es einige Herausforderungen bei der Koordination und Verknüpfung verschiedener Klassen und Interfaces. Es war wichtig sicherzustellen, dass die Schnittstellen korrekt implementiert wurden.

### Wie habe ich auf die Schwierigkeiten reagiert?

Um die Schwierigkeiten zu bewältigen, habe ich gründlich dokumentiert und sicherheitsrelevante Tests durchgeführt. Auch der Austausch mit Kollegen half mir, Lösungen für auftretende Probleme zu finden.

### Folgende Arbeiten habe ich ausgeführt:

- Erweiterung zum Thema [[12_Interfaces|Interfaces]]
- Interfaces in einem [[12_Interfaces#Arbeitsblatt Game|Game]] implementiert
- Weiteres Arbeiten am ePortfolio

## Reflexion:

### Was ist mir heute klar geworden?

Heute wurde mir klar, dass Interfaces ein leistungsstarkes Werkzeug in der Softwareentwicklung sind und in einer Vielzahl von Anwendungsfällen eingesetzt werden können. Sie ermöglichen eine effiziente Kommunikation zwischen verschiedenen Klassen und eröffnen Möglichkeiten zur Erweiterung und Modularisierung von Code.

### Was hat mich emotional angesprochen, bewegt, verunsichert, etc.?

Die praktische Umsetzung von Interfaces in einem realen Game hat mich motiviert und gezeigt, wie abstrakte Konzepte in der Programmierung tatsächlich funktionieren. Es war eine befriedigende Erfahrung, den Code in Aktion zu sehen.

### Was nehme ich in den Berufsalltag mit?

Ich nehme mit, wie wichtig es ist, Interfaces in der Softwareentwicklung gezielt einzusetzen. Sie bieten eine Möglichkeit zur effizienten Zusammenarbeit von Klassen und tragen zur Modulbildung und Wartbarkeit des Codes bei.

### Welche Fragen sind heute aufgetaucht?

An diesem Tag sind keine spezifischen Fragen aufgetaucht, da ich erfolgreich an der Umsetzung der Konzepte gearbeitet habe.