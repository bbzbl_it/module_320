# Woche 4 // Lerntagebuch 06 09 2023

Lektionen:

- 08:05 - 11:35


### Was habe ich gemacht? 
Heute habe ich gemeinsam mit einem Partner den Code des Invoice Program überprüft und analysiert. Wir haben verschiedene Lösungsansätze verglichen und diskutiert. Anschließend haben wir uns E-Portfolios und Schaufenster näher angesehen.

Danach haben wir die Grundlagen von Javadoc kennengelernt, einer wichtigen Methode zur Dokumentation von Code in der Softwareentwicklung. Wir hatten einen Javadoc-Auftrag zu erledigen, bei dem wir Code kommentieren und dokumentieren mussten. Zum Abschluss des Tages habe ich mein Lerntagebuch aktualisiert.


### Was habe ich gelernt? 
Heute habe ich gelernt, wie wertvoll die Zusammenarbeit mit einem Partner bei der Codeanalyse sein kann. Durch den Vergleich verschiedener Lösungsansätze konnten wir besser verstehen, wie man effizienten und strukturierten Code schreibt. Ebenso habe ich erfahren, wie E-Portfolios und Schaufenster genutzt werden können. Die Grundlagen von Javadoc haben mir gezeigt, wie wichtig eine ordnungsgemäße Dokumentation in der Softwareentwicklung ist.


### Was ist mir gut gelungen? 
Die Zusammenarbeit mit meinem Partner beim Codevergleich ist gut gelungen. Wir konnten unsere jeweiligen Ansätze diskutieren und voneinander lernen. Die Bearbeitung des Javadoc-Auftrags ist mir ebenfalls gut gelungen, da ich die Prinzipien der Javadoc-Dokumentation erfolgreich angewendet habe.


### Wie bin ich vorgegangen, dass es mir gelungen ist? 
Bei der Codeanalyse habe ich offen mit meinem Partner kommuniziert und die Vor- und Nachteile der verschiedenen Lösungsansätze gründlich besprochen. Beim Javadoc-Auftrag habe ich die Javadoc-Dokumentation studiert und sicherstellen können, dass meine Dokumentation den Anforderungen entspricht.


### Womit hatte ich Schwierigkeiten? 
Während der Codeanalyse hatten wir einige Schwierigkeiten, da wir unterschiedliche Ansätze verfolgten. Dies erforderte eine offene Kommunikation und Diskussion. Anfangs hatte ich Schwierigkeiten bei der korrekten Anwendung von Javadoc, da es einige spezifische Regeln und Tags gibt, die beachtet werden müssen.


### Wie habe ich auf die Schwierigkeiten reagiert? 
Um die Schwierigkeiten beim Codevergleich zu überwinden, haben mein Partner und ich aktiv miteinander kommuniziert und die Vor- und Nachteile der verschiedenen Ansätze abgewogen. Bei den Schwierigkeiten mit Javadoc habe ich die Javadoc-Dokumentation sorgfältig studiert und Beispiele analysiert, um sicherzustellen, dass meine Dokumentation den Standards entspricht.


### Folgende Arbeiten habe ich ausgeführt: 
- [[07_Modellieren und Implementieren#Invoice|Invoice Program]] oder [[07_Modellieren und Implementieren#Game|Game]] mit Partner vergleichen
- E-Portfolios / Schaufenster ansehen
- Theorie zu [[08_Javadoc#Präsentation|Javadoc]]
- [[08_Javadoc#Aufgabe|Javadoc Auftrag]] ererledigen
- [[08_Javadoc#Lösung|Javadoc Lösung]] mit Musterlösung vergleichen
- Lerntagebuch Fortführen



## Reflexion:


### Was ist mir heute klar geworden? 
Mir ist heute klar geworden, wie wichtig die Zusammenarbeit bei der Codeanalyse ist und wie wertvoll Feedback von anderen Entwicklern sein kann. Ebenso habe ich erkannt, dass die richtige Dokumentation von Code, insbesondere mit Javadoc, in der Softwareentwicklung von großer Bedeutung ist. Die Nutzung von E-Portfolios zur Entwicklung ist eine nützliche Praxis.


### Was hat mich emotional angesprochen, bewegt, verunsichert, etc.? 
An diesem Tag gab es keine Unsicherheiten.


### Was nehme ich in den Berufsalltag mit? 
Ich nehme mit, wie wichtig Zusammenarbeit und Dokumentation in der Softwareentwicklung sind. 


### Welche Fragen sind heute aufgetaucht?
An diesem Tag sind keine spezifischen Fragen aufgetaucht.




