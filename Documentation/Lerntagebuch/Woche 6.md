# Woche 6 // Lerntagebuch 20 09 2023

Lektionen:

- 08:05 - 11:35


### Was habe ich gemacht?

In der heutigen Lektion habe ich meine Lernreise fortgesetzt und mich intensiv mit abstrakten Klassen und Interfaces in der objektorientierten Programmierung beschäftigt.

### Was habe ich gelernt?

Zuerst habe ich ein Lernvideo zum Thema "Abstrakte Klassen" angesehen, um die Grundlagen dieser Konzepte zu verstehen. Anschließend habe ich das Thema "Abstrakte Klassen" durch das Studium eines Factsheets vertieft.

Nachdem ich ein solides Verständnis für abstrakte Klassen entwickelt hatte, habe ich diese Konzepte praktisch umgesetzt. Ich habe abstrakte Klassen in mein Auto-Programm integriert und konnte dadurch eine bessere Struktur und Wartbarkeit erzielen.

Als nächstes habe ich mich dem Thema "Interfaces" gewidmet. Ich habe das Konzept von Interfaces studiert und Aufgaben im Zusammenhang damit gelöst. Dabei war es wichtig, die Unterschiede zwischen abstrakten Klassen und Interfaces zu verstehen und deren Verwendungszwecke zu erkennen.

Schließlich habe ich die Konzepte von Interfaces in einem Game implementiert, um ihr Potenzial in der Praxis zu erleben.

### Was ist mir gut gelungen?

Die Implementierung von abstrakten Klassen in meinem Auto-Programm verlief erfolgreich und hat die Struktur des Codes verbessert. Auch die Umsetzung von Interfaces in einem Game konnte ich effektiv durchführen.

### Wie bin ich vorgegangen, dass es mir gelungen ist?

Ich bin systematisch vorgegangen, indem ich zuerst das theoretische Wissen durch das Lernvideo und das Factsheet erworben habe. Dann habe ich die Konzepte praktisch umgesetzt, wobei ich darauf geachtet habe, klare Schnittstellen und Vererbungshierarchien zu erstellen.

### Womit hatte ich Schwierigkeiten?

Während der Implementierung von Interfaces gab es einige Herausforderungen, insbesondere beim Entwurf der Schnittstellen und der Anpassung des Codes in einem Game. Auch das Verständnis der feinen Unterschiede zwischen abstrakten Klassen und Interfaces erforderte genaue Überlegungen.

### Wie habe ich auf die Schwierigkeiten reagiert?

Um die Schwierigkeiten zu bewältigen, habe ich zusätzliche Ressourcen konsultiert und mich mit meinen Kollegen ausgetauscht. Wir haben gemeinsam an den Herausforderungen gearbeitet und Lösungen gefunden, um abstrakte Klassen und Interfaces effektiv zu implementieren.
### Folgende Arbeiten habe ich ausgeführt: 
- Lerntagebuch fortgeführt
- Lernvideo zum Thema [[11_Abstrakte Klassen#Lernvideo|Abstrakte Klassen]] angesehen
- Thema [[11_Abstrakte Klassen#Factsheet|Abstrakte Klassen]] studieren
- [[11_Abstrakte Klassen#Arbeitsblatt|Abstrakte Klassen]] in Auto Program implementieren
- Thema [[12_Interfaces#Einführung|Interfaces]] studieren
- Aufgaben zu [[12_Interfaces#Arbeitsblatt|Interfaces]] lösen und vergleichen
- [[12_Interfaces#Arbeitsblatt Game|Interfaces]] in einem Game implementieren

## Reflexion:

### Was ist mir heute klar geworden?

Mir wurde heute noch deutlicher bewusst, wie abstrakte Klassen und Interfaces in der objektorientierten Programmierung genutzt werden können, um flexiblen und gut strukturierten Code zu erstellen. Diese Konzepte bieten wichtige Werkzeuge zur Verbesserung der Codequalität.

### Was hat mich emotional angesprochen, bewegt, verunsichert, etc.?

Während des Lernens und der praktischen Umsetzung gab es keine besonderen emotionalen Reaktionen oder Unsicherheiten.

### Was nehme ich in den Berufsalltag mit?

Ich nehme mit, wie wertvoll abstrakte Klassen und Interfaces in der Softwareentwicklung sind. Sie ermöglichen es, Code effizienter und modularer zu gestalten und erleichtern die Zusammenarbeit im Team.

### Welche Fragen sind heute aufgetaucht?

An diesem Tag sind keine spezifischen Fragen aufgetaucht, da ich durch die Zusammenarbeit mit meinen Kollegen viele meiner Fragen klären konnte.