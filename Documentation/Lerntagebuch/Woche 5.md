# Woche 5 // Lerntagebuch 13 09 2023

Lektionen:

- 08:05 - 11:35

### Was habe ich gemacht?

Heute habe ich mich intensiv mit den Themen [[09_Vererbung|Vererbung]] und [[10_Polymorphie|Polymorphie]] auseinandergesetzt.

### Was habe ich gelernt?

Während dieser Lektionen habe ich mich eingehend mit dem grundlegenden Konzept der [[09_Vererbung|Vererbung]] beschäftigt. Ich habe ein Lernvideo zu diesem Thema angesehen, um ein tieferes Verständnis zu entwickeln. Anschließend habe ich die [[09_Vererbung|Vererbung]] in Java implementiert, um die Theorie in die Praxis umzusetzen.

Darüber hinaus habe ich das Thema [[10_Polymorphie|Polymorphie]] erkundet. Ein weiteres Lernvideo half mir dabei, dieses Konzept zu verstehen, und ich habe es ebenfalls in meiner Java-Umgebung implementiert.

### Was ist mir gut gelungen?

Ich habe das Konzept der [[09_Vererbung|Vererbung]] erfolgreich in meinem [[00_CODE|Java-Code]] implementiert und konnte die Beziehungen zwischen den [[01_Klassen und Objekte|Klassen]] klar definieren. Auch die Umsetzung der [[10_Polymorphie|Polymorphie]] in meinem Code ist mir gut gelungen, und ich kann nun unterschiedliche Objekte in einer generischen Art und Weise behandeln.

### Wie bin ich vorgegangen, dass es mir gelungen ist?

Um die [[09_Vererbung|Vererbung]] und [[10_Polymorphie|Polymorphie]] erfolgreich umzusetzen, habe ich systematisch vorgegangen. Zuerst habe ich das theoretische Wissen durch ein [[09_Vererbung#Lernvideo|Lernvideo]] aufgefrischt und vertieft. Dann habe ich die Konzepte in meiner Entwicklungsumgebung praktisch umgesetzt, wobei ich auf klare Klassenstrukturen und Vererbungshierarchien geachtet habe.

### Womit hatte ich Schwierigkeiten?

Während der Implementierung der [[09_Vererbung|Vererbung]] und [[10_Polymorphie|Polymorphie]] hatte ich einige Schwierigkeiten mit komplexeren Klassenstrukturen und der Handhabung von Vererbungskonflikten. Insbesondere die Auswahl der richtigen Konzepte für die [[10_Polymorphie|Polymorphie]] konnte anspruchsvoll sein.

### Wie habe ich auf die Schwierigkeiten reagiert?

Um die Schwierigkeiten zu bewältigen, habe ich zusätzliches Material studiert und mich mit den Kollegen ausgetauscht. Wir haben gemeinsam an den Herausforderungen gearbeitet und Lösungen gefunden, um die [[09_Vererbung|Vererbung]] und [[10_Polymorphie|Polymorphie]] effektiv zu implementieren.

### Folgende Arbeiten habe ich ausgeführt:

- Lerntagebuch fortgeführt
- Thema [[09_Vererbung#Reale Objekte|Vererbung]] Grundgedanke
- Lernvideo zum Thema [[09_Vererbung#Lernvideo|Vererbung]] angesehen
- [[09_Vererbung#Vererbung implementieren|Vererbung]] in Java implementiert
- Thema [[10_Polymorphie#Präsentation|Polymorphie]] verstehen
- Lernvideo zum Thema [[10_Polymorphie|Polymorphie]] angesehen
- [[10_Polymorphie#Arbeitsblatt|Polymorphie]] in Java implementiert

## Reflexion:

### Was ist mir heute klar geworden?

Mir wurde heute klar, wie mächtig die Konzepte der [[09_Vererbung|Vererbung]] und [[10_Polymorphie|Polymorphie]] in der objektorientierten Programmierung sind. Sie ermöglichen es, den Code flexibler und wartbarer zu gestalten.

### Was hat mich emotional angesprochen, bewegt, verunsichert, etc.?

Während des Lernens und der praktischen Umsetzung dieser Konzepte gab es keine Unsicherheiten.

### Was nehme ich in den Berufsalltag mit?

Ich nehme mit, wie wichtig es ist, die Prinzipien der [[09_Vererbung|Vererbung]] und [[10_Polymorphie|Polymorphie]] zu verstehen und in der Praxis anzuwenden. Diese Konzepte sind entscheidend für die Entwicklung von robusten und flexiblen Softwarelösungen.

### Welche Fragen sind heute aufgetaucht?

An diesem Tag sind keine spezifischen Fragen aufgetaucht.