# Woche 2 // Lerntagebuch 23 08 2023

Lektionen:

- 08:05 - 11:35


### Was habe ich gemacht? 

Zu begin haben wir die Hausaufgaben zum Thema Kapselung angesehen. Als das geklärt war, gab es eine kleine Vertiefung zur Kapselung gegeben. 
Danach wurden uns Konstruktoren erklärt und es gab eine Aufgabe dazu diese in die Account Application zu implementieren.
Jetzt haben wir alle Komponenten gelernt, die man kennen muss um UML Klassendiagramme zu verstehen und gebrauche. Deshalb wurde uns dies auch gleich vorgeführt.

Nach der Pause ging es auch gleich weiter. 
Zu diesem Zeitpunkt, haben wir den Auftrag Regatta erhalten.
Diesen haben wir dann bis um 11:25 Uhr bearbeitet.
Zum Schluss gab es noch eine kleine Besprechung und uns wurde die Musterlösung zu dieser Aufgabe erklärt.
Danach hatten wir noch etwas Zeit das Lerntagebuch zu erweitern.


### Was habe ich gelernt? 

Wen man manuell Konstruktoren erstellt aber den Standard Konstruktor behalten möchte, muss man diesen auch im Code definiert haben.  
Ich habe gelernt wie man OML erstellt und wie man ein solches als Code implementiert.


### Was ist mir gut gelungen? 

Das erstellen und verlinken und verwenden von mehreren Fachklassen im gleichen Classpath.
Die Hausaufgaben die ich gemacht habe sind sehr einfach und gut gelaufen.


### Wie bin ich vorgegangen, dass es mir gelungen ist? 

Ich habe die Anweisungen und Konzepte sorgfältig verstanden und systematisch in meinem Projekt umgesetzt. Das Verwenden von Fachklassen und das Umsetzen der Aufgaben erfolgte schrittweise und gut strukturiert.


### Womit hatte ich Schwierigkeiten? 

Während der Konstruktor-Implementierung hatte ich anfangs Schwierigkeiten, den Zusammenhang zwischen den verschiedenen Konstruktorarten zu verstehen. 


### Wie habe ich auf die Schwierigkeiten reagiert? 

Um die Schwierigkeiten zu überwinden, habe ich zusätzlich im Internet recharchiert. Zudem habe ich meine Fragen während der Unterricht gestellt.


### Folgende Arbeiten habe ich ausgeführt: 

- Hausaufgaben [[02_Kapselung#Kapselung beurteilen|Kapselung]] ansehen
- [[02_Kapselung#Kapselung|Kapselung]] erweitert ansehen
- [[03_Konstruktor#Skript Konstruktor|Script zu Konstruktoren]]
- [[04_UML#Klassendiagramm erstellen|UML]] Klassendiagramme nach Auftrag erstellen
- [[05_Mehrere Fachklassen#Arbeitsblatt Regatta|Regatta Program]] von UML nach Code implementieren
- Lerntagebuch Fortführen



## Reflexion:


### Was ist mir heute klar geworden? 

Das UML Diagrammen sich sehr mit ERM Diagrammen ähneln.


### Was hat mich emotional angesprochen, bewegt, verunsichert, etc.? 

Im Moment nichts


### Was nehme ich in den Berufsalltag mit? 

Das OOP kann sehr einfach durch UML Diagramme konzeptiert werden.
Und Dokumentation ist allgemein wichtig


### Welche Fragen sind heute aufgetaucht?

Im Moment keine