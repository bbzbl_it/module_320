# HZ2 - Objektorientiert modellieren

## Handlungsziel 

Modelliert und dokumentiert objektorientierte Programme 

## Theorie

| Theorie | Ressourcen | Zusammenfassung |
|---|---|---|
| UML | [[04_UML#Präsentation]] <br> [Wikipedia](https://de.wikipedia.org/wiki/Unified_Modeling_Language) <br> [Cheat Sheet](https://khalilstemmler.com/articles/uml-cheatsheet/) | Die Unified Modeling Language ist ein Standard zur graphischen Darstellung von Klassen o.ä. in Form eines Diagramm. Es werden Klassen, Instanzvariabeln und Methoden sowie deren Beziehungen und Sichtbarkeiten dargestellt.|


## Kompetenzen 

### B1G

Ich kann Klassen inkl. Attribute und Methoden in einem UML Klassendiagramm  darstellen. 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [[04_UML#Klassendiagramm erstellen|Auftrag UML]]

Für diesen Aufrag durften wir in Teams arbeiten. Das Ziel ist es, die vorgegebene Klasse die in einem Text erklärt wird, in ein UML Diagram umzuwandeln. 
Hier ist meine  [[04_UML#Lösung|Lösung]]. In dieser kann man sehen, das wir aus der Textaufgabe drei folgende Instanzvariablen herausgelesen haben. Diese sind `number`, `name` und `time`.  In java, diese Instanzvariablen haben die Sichtbarkeit `private` dies wird durch die `-` im UML dargestelt. Folgend ist der Name der Vatiable. Nach dem `:` kommt dann der Datentyp. Darunter in einer "nächsten Box" sind dann Methoden angegeben. In diesem fall gibt es dann 6 Methoden, jeweils einen getter und setter pro Variable. Das spezielle am Setter für dir Variabe `time` ist, das es in die `race` Methode umbenant wurde und keinen Input verlange, sondern die Variable `time` zufällig auf einen Wert zwischen 300 und 600 setzt.
Die Methoden sind wie folgt in UML aufgebaut.

```
[+/-]<Name>([<Name> : <typ>[, ...]])[ : <typ>]

^     ^    ^  ^        ^       ^         ^ 
|     |    |  |        |       |         | 
|     |    |  |        |       |         Typ des Rückgabe Werts 
|     |    |  |        |       evtl. weitere Eingabe Variablen
|     |    |  |        Typ der Eingabe Variable
|     |    |  Name der Eingabe Variable
|     |    Klammer der Funktion (gleich wie in Java)
|     Name der Methode
Sichbarkeit
```

### B1F

Ich kann Klassen und deren Beziehungen untereinander in einem UML Klassendiagramm darstellen. (Assoziationen) 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewiesen: [[07_Modellieren und Implementieren#Meine Lösung|UML zum Invoice Programm]]

Diesen Auftrag habe ich im Team ausgeführt, wie ich beim schreiben des Sachaufensters feststellen musste, nachdem ich 30 min lang nach der Lösung gesucht habe und sie anschliessend im "Platz für Zusammenarbeit" auf OneNote gefunden habe.

Der Auftrag war es ein Invoice Programm zu erstellen das die Klassen `Invoice` und  `InvoiceItem` hat. Dazu musste man auch noch ein UML erstellen. Im UML kann man die Klasse InvoiceItem rechts erkennen. Diese Klasse ist ein ganz normales UML wie es weiter oben auf dieser Seite schon erklärt wurde. Neu ist jetzt allerdings die 2. Klasse und der Pfeil der die zwei Klassen verbindet. zuerst zu der rechten Klasse.
Diese Klasse weist auch die einige Ähnlichkeiten mit den UMLs auf die wir von oben kennen, jedoch ist etwas sehr speziell. Und zwar zeigt diese Klasse eine Instanzvariable auf, die den Typ der Klasse auf, die der Pfeil mit dieser Klasse verbindet. In diesem Fall haben wir eine `ArrayList` mit dem Typ `InvoiceItem` in der Klasse `Invoice`. Doch da diese Instanzvariable `private` ist, haben wir mehrere Methoden implementiert die mit dieser `ArrayList` interagieren. Darunter z.B. `addItem()` oder `removeItem()`. Diese interagieren mit der ArrayList.
Doch nun zu dem Pfeil. Dieser Pfeil beschreibt die Beziehung zwischen den beiden Klassen. der Pfeil besteht aus folgenden Bausteinen:

| Symbol | Bedeutung |
|---|---|
| Linie | Die Linie die die beiden Klassen verbindet. |
| `X` | Das X Beschreibt, das diese Seite der Beziehung nicht von der anderen Klasse referenziert wird. |
| `>` | Das > Beschreibt, das diese Klasse von der anderen Klasse referenziert wird. |
| `0..*` | Dieses Symbol beschreibt die "Menge" der Beziehung. in diesem fall ist es eine 0 bis Mehrere Beziehung. Es gibt aber auch noch andere. z.B. 0..1 1..* etc. |



### B1E

Ich kann Klassen und Interfaces inkl. Vererbungshierarchie in einem UML Klassendiagramm darstellen.

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag nachgewiesen: [[04_UML#Auftrag|Interfaces in UML]]

In diesem Auftrag war es erforderlich, eine UML-Darstellung für eine Hierarchie von Waffen für ein Spiel zu erstellen, die sowohl Klassen als auch Interfaces umfasst.

Die Klassendiagramme in UML repräsentieren die verschiedenen Waffen und Interfaces. Die Vererbungshierarchie wird durch Pfeile zwischen den Klassen und Interfaces dargestellt, wobei der Pfeil je nach Art der Beziehung gestrichelt oder gezogen ist. Das JAVA-Keyword für die gezogene Linie ist `extends` und für gestrichelt ist es `implements`. 
Intefaces in UML werden wie Klassen behandelt, jedoch wird ein `<<Interface>>` in der 1. Linie geschrieben und der Name des Interfaces darunter.
