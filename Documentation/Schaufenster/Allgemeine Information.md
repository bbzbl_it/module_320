# Allgemeine Information

Im Schaufenster kombinieren Sie Ihre gesammelten Ressourcen (Teil 1) mit Ihrem Lerntagebuch (Teil 2). Sie zeigen Ihre jeweils wichtigsten Erkenntnisse, Erfahrungen und Ergebnisse in Bezug auf die Kompetenzen des Kompetenzrasters auf und beschreiben diese nachvollziehbar. Abschliessend blicken Sie noch einmal auf alle erarbeiteten Modulinhalte zurück und verfassen ein Schlusswort in welchem Sie darauf eingehen, was Sie jetzt mehr wissen und können als vor dem Modul.  

Mit diesem Teil geben Sie Einblick in Ihre persönliche Kompetenzerreichung.

