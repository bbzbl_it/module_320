
# Selbständigkeitserklärung

Hiermit erkläre ich, dass ich die vorliegende Arbeit selbständig angefertigt und keine anderen als die angegebenen Hilfsmittel verwendet habe. Sämtliche verwendeten Textausschnitte, Zitate oder Inhalte anderer Verfasser wurden ausdrücklich als solche gekennzeichnet. 

Sissach, 08.10.2023 

| Name | Unterschrift |
|---|---|
|Joshua Kunz| ![[Unterschrift.png]] |

