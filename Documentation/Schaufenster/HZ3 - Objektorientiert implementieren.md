# HZ3 - Objektorientiert implementieren

## Handlungsziel 

Implementiert objektorientiertes Design 

## Theorie

|Theorie|Ressourcen|Zusammenfassung|
|---|---|---|
|Kapselung|- [[02_Kapselung#Kapselung]] <br> - [Webseite](https://simpleclub.com/lessons/fachinformatikerin-datenkapselung-in-java)|Kapselung ist das Zuweisen von Sichtbarkeit an die Variablen und Methoden einer Klasse. Die Idee ist es, die Variablen so zu schützen, dass sie nur durch eine `setter` Methode verändert werden können, die möglicherweise den neuen Wert validieren kann oder ähnliches. Diese `setter` und `getter` Methoden sind deshalb öffentlich und können von allen aufgerufen werden.|
|Konstruktor|- [[03_Konstruktor#Skript Konstruktor]] <br> - [Webseite](https://www.geeksforgeeks.org/constructors-in-java/)|Ein Konstruktor in Java ist eine spezielle Methode, die verwendet wird, um ein Objekt einer Klasse zu initialisieren. Konstruktoren haben denselben Namen wie die Klasse selbst und haben keinen Rückgabetyp. Sie werden in der Regel verwendet, um die Eigenschaften eines Objekts beim Erstellen festzulegen.|
|Vererbung|- [[09_Vererbung#Reale Objekte]] <br> - [Webseite](https://www.javatpoint.com/inheritance-in-java)|Vererbung ist ein Konzept in der objektorientierten Programmierung, das es einer Klasse ermöglicht, die Eigenschaften und Methoden einer anderen Klasse zu erben. Dies ermöglicht die Wiederverwendung von Code und die Erstellung von Hierarchien von Klassen.|
|Polymorphie|- [[10_Polymorphie#Präsentation]] <br> - [Webseite](https://www.geeksforgeeks.org/polymorphism-in-java/)|Polymorphie ist ein Konzept, bei dem ein Objekt verschiedene Formen annehmen kann. In Java wird Polymorphie durch Methodenüberschreibung und Schnittstellen erreicht. Es ermöglicht, dass verschiedene Klassen dieselbe Methode anders implementieren.|
|Abstrakte Klassen|- [[11_Abstrakte Klassen#Factsheet]] <br> - [Webseite](https://www.tutorialspoint.com/java/java_abstraction.htm)|Eine abstrakte Klasse in Java ist eine Klasse, die nicht instanziiert werden kann und in der Regel einige abstrakte Methoden enthält, die von abgeleiteten Klassen implementiert werden müssen. Sie dient als Vorlage für andere Klassen und kann nicht direkt verwendet werden.|
|Interfaces|- [[12_Interfaces#Einführung]] <br> - [Webseite](https://www.geeksforgeeks.org/interfaces-in-java/)|Ein Interface in Java ist eine Sammlung von abstrakten Methoden, die von implementierenden Klassen implementiert werden müssen. Es ermöglicht die Definition von Verträgen, die Klassen erfüllen müssen, und ermöglicht eine lose Kopplung zwischen verschiedenen Teilen des Codes.|


## Kompetenzen 

### D1G

Ich kann Klassen mit Attributen, Konstruktoren und Methoden implementieren und davon Objekte erzeugen. 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_2/SailingRace)
In diesem Auftrag musste ich Klassen mit Attributen, Konstruktoren und Methoden implementieren und davon Objekte erzeugen. Ein Beispiel dafür ist die Klasse `Ship`, die ich erstellt habe. Diese Klasse verfügt über Attribute wie `ID`, `Name` und `Zeit`. Der Konstruktor der Klasse setzt die Werte für `ID` und `Name`, und es gibt auch setter- und getter-Methoden für diese Attribute. Die Methode `Race` erzeugt eine zufällige Zeit für das Schiff, um das Rennen zu simulieren.

Ein weiteres Beispiel ist die Klasse `Competition`, die ebenfalls in diesem Auftrag erstellt wurde. Diese Klasse hat Attribute wie `Name` und ein Array vom Typ `Ship`. Der Konstruktor setzt den Namen des Rennens, und es gibt Methoden, um Schiffe zum Rennen hinzuzufügen und das Rennen zu starten. Dabei wurden Konstruktoren, Attribute und Methoden gekonnt implementiert, um die gewünschte Funktionalität zu erreichen.


### D1F

Ich kann Beziehungen zwischen Klassen implementieren. 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_5/Polymorphie)
In diesem Auftrag musste ich Beziehungen zwischen Klassen implementieren, insbesondere wurde die Vererbung und Polymorphie verwendet. Ein herausragendes Beispiel hierfür ist die Umsetzung von verschiedenen Formen in der Objektorientierten Programmierung.

Ich habe eine Basisklasse `Shape` erstellt, die grundlegende Eigenschaften und Methoden für Formen definiert. Darauf aufbauend habe ich Unterklassen wie `Circle`, `Rectangle` und `Triangle` erstellt, die spezifische Eigenschaften und Methoden für diese Formen implementieren. Diese Unterklassen erben von der Basisklasse `Shape` und erweitern sie.

Ein weiteres Beispiel ist die Verwendung von Polymorphie, bei der ich eine Liste von verschiedenen Formen erstellt habe und auf sie in einer vereinheitlichten Weise zugegriffen habe, unabhängig von ihrer spezifischen Klasse. Dies ermöglichte es mir, die gleiche Methode auf v




### D1E

Ich kann Interaktion zwischen den Objekten implementieren. 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_3/InvoiceProgram)
In diesem Auftrag musste ich die Fähigkeit zur Implementierung von Interaktionen zwischen Objekten demonstrieren. Ein konkretes Beispiel hierfür ist die Entwicklung eines Rechnungsprogramms.

Ich habe eine Klasse `Invoice` erstellt, die Rechnungen repräsentiert und Attribute wie Rechnungsnummer, Rechnungsdatum und Gesamtbetrag enthält. Zusätzlich habe ich eine Klasse `Item` erstellt, die die einzelnen Positionen auf der Rechnung darstellt, einschließlich Produktname, Preis und Menge.

Die Interaktion zwischen diesen Objekten wurde durch die Implementierung von Methoden ermöglicht, die es ermöglichen, Artikel zur Rechnung hinzuzufügen, den Gesamtbetrag zu berechnen und Rechnungsinformationen anzuzeigen. Diese Methoden erlaubten es, Daten zwischen den Objekten auszutauschen und die gewünschten Funktionalitäten des Rechnungsprogramms zu realisieren.



### D2G

Ich kann Vererbung implementieren und dessen Einsatz begründen. 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_5/Vererbung)
Im bereitgestellten Code wurden Vererbungskonzepte erfolgreich implementiert und deren Einsatz begründet. Hierzu wurden drei Klassen verwendet:

1. `Car` ist die Basisklasse, die grundlegende Eigenschaften eines Autos wie das Modell und den Preis definiert. Die Methode `getCarPrice()` gibt den Preis des Autos zurück.
2. `CrashedCar` erbt von der Klasse `Car` und erweitert sie. Diese Klasse fügt ein zusätzliches Attribut `damageLevel` hinzu, um den Schadensgrad des Autos zu speichern. Die Methode `getPrice()` wurde überschrieben, um den Preis eines Unfallwagens basierend auf dem Schadensgrad anzupassen. Dies ist ein Beispiel für die Vererbung, bei der eine abgeleitete Klasse zusätzliche Funktionalitäten zur Verfügung stellt.
3. `UsedCar` ist eine weitere abgeleitete Klasse von `Car`. Sie enthält das Attribut `mileage`, um die Kilometerleistung des Autos zu verfolgen. Die Methode `getPrice()` wurde erneut überschrieben, um den Preis eines Gebrauchtwagens basierend auf der Kilometerleistung anzupassen.

Der Einsatz von Vererbung in diesem Kontext bietet klare Vorteile. Die gemeinsamen Eigenschaften und Methoden der Autos, die in der Basisklasse `Car` definiert sind, müssen nicht in den abgeleiteten Klassen `CrashedCar` und `UsedCar` wiederholt werden. Stattdessen können sie geerbt und bei Bedarf angepasst oder erweitert werden. Dies führt zu sauberem und wartbarem Code, da Redundanz vermieden wird.



### D2F

Ich kann statische und dynamische Polymorphie implementieren. 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_5/Polymorphie)
Statische Polymorphie (Überladen von Methoden) wurde in den Klassen `CrashedCar` und `UsedCar` verwendet, indem die Methode `getPrice()` in beiden Klassen überschrieben wurde. Jede dieser Klassen definiert eine eigene Version der Methode `getPrice()`, die spezifisch für den Zustand des Autos ist. Dies ermöglicht es, denselben Methodennamen für verschiedene Arten von Autos zu verwenden, während die spezifische Implementierung je nach Zustand des Autos unterschiedlich ist.

Dynamische Polymorphie wurde in der Klasse `Fleet` demonstriert, indem eine Liste von Objekten der Basisklasse `Car` verwaltet wurde. Obwohl die Liste mit verschiedenen Arten von Autos gefüllt werden kann, konnte die Methode `print()` auf alle Objekte in der Liste angewendet werden, unabhängig von ihrem tatsächlichen Typ. Dies ist möglich, weil die Methode `getPrice()` zur Laufzeit dynamisch aufgerufen wird, basierend auf dem tatsächlichen Typ jedes Autos in der Liste.

Die Verwendung von statischer und dynamischer Polymorphie in diesem Auftrag trägt dazu bei, den Code effizienter und flexibler zu gestalten. Die statische Polymorphie ermöglicht es, Methoden übersichtlich zu organisieren und zu benennen, während die dynamische Polymorphie die Interaktion mit verschiedenen Objekten erleichtert, ohne sich um deren konkrete Implementierungsdetails kümmern zu müssen.




### D2E

Ich kann die Abstraktion mit abstrakten Klassen und Interfaces weiter erhöhen, diese Elemente implementieren und den Einsatz begründen.

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_6/Abstrakte%20Klassen)
Im Code wurde die Abstraktion mit abstrakten Klassen und Interfaces erhöht, um die Flexibilität und Erweiterbarkeit des Codes zu verbessern. Dabei wurden folgende Klassen und Interface verwendet:

1. `IWeapon` ist ein Interface, das die Funktionalität aller Waffen beschreibt, die ein Charakter tragen kann. Es enthält die Methode `use(int ap)`, die die Verwendung einer Waffe basierend auf den Angriffspunkten des Angreifers beschreibt.
2. `Axe` und `Dagger` sind Implementierungen des `IWeapon`-Interfaces und repräsentieren zwei verschiedene Arten von Waffen. `Axe` führt einen zufälligen Angriff aus, während `Dagger` zwei zufällige Angriffe ausführt und die Ergebnisse zusammenzählt.
3. `Character` ist eine Klasse, die einen Charakter im Spiel darstellt. Diese Klasse verwendet das abstrakte Konzept eines "IWeapons", das ein Charakter tragen kann. Durch die Verwendung des `IWeapon`-Interfaces kann der Charakter jede beliebige Waffe tragen, indem die Methode `setWeapon(IWeapon weapon)` aufgerufen wird.
4. `Fight` ist eine Klasse, die einen Kampf zwischen zwei Charakteren verwaltet. Die Charaktere können unterschiedliche Waffen tragen, die das `IWeapon`-Interface implementieren. Dadurch können verschiedene Arten von Waffen im Kampf eingesetzt werden, ohne den Code der `Fight`-Klasse ändern zu müssen.

Der Einsatz von abstrakten Klassen und Interfaces erhöht die Abstraktion und Flexibilität des Codes erheblich. Abstrakte Klassen und Interfaces ermöglichen es, ohne die konkrete Implementierung zu spezifizieren. Dies ermöglicht es, verschiedene Implementierungen für dieselbe Schnittstelle zu erstellen und leicht zwischen ihnen zu wechseln, was die Wiederverwendbarkeit und Erweiterbarkeit des Codes verbessert.


