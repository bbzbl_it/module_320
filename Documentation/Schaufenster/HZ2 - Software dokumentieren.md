# HZ2 - Software dokumentieren

## Handlungsziel 

Modelliert und dokumentiert objektorientierte Programme 

## Theorie

| Theorie | Ressourcen | Zusammenfassung |
|---|---|---|
| JavaDoc | [[08_Javadoc#Präsentation]] <br> [Wikipedia](https://de.wikipedia.org/wiki/Javadoc)  <br> [Cheat Sheet](https://binfalse.de/2015/10/05/javadoc-cheats-sheet/) | Javadoc ist ein Tool in der Java-Programmierung, das automatisch Quellcode-Kommentare in nützliche HTML-Dokumentation umwandelt. Es verbessert die Verständlichkeit und Dokumentation von Java-Code. |


## Kompetenzen 

### C1G
Ich kann erklären, wozu ein Softwaredokumentationswerkzeug (Javadoc) dient. 

#### Umsetzung
Ein Softwaredokumentationswerkzeug wie Javadoc dient dazu, den Java-Quellcode zu kommentieren und automatisch eine gut strukturierte Dokumentation zu generieren. Diese Dokumentation enthält Informationen über Klassen, Methoden, Variablen und mehr. Es ermöglicht Entwicklern, ihren Code zu dokumentieren, sodass andere Entwickler verstehen können, wie er funktioniert und wie er verwendet werden sollte. Dies erleichtert die Wartung, Zusammenarbeit und Nutzung von Java-Code, da die Dokumentation als Referenz für Entwickler und Benutzer dient.


### C1F
Ich kann Software mit Hilfe von Javadoc im Rahmen von einfachen Kommentaren dokumentieren. 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewiesen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_4/Javadoc/Start.java) 
Da ich mir nicht sicher bin, ob die Kompetenz Richtiges JavaDoc oder nur "einfache Kommentare"
Deshalb gehe ich von einfachen Kommentaren aus.
Deshalb sieht man im angegebenen Code das alle Linien des Codes Beschreiben was sie gerade Tun. Ich denke diese Kompetenz ist nicht schweer.

### C1E
Ich kann Software mit Hilfe von Javadoc und dessen wichtigsten Tags dokumentieren.

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_4/Javadoc)
Dieser Auftrag ist sehr einfach. Ich habe den alten Code [[08_Javadoc#Aufgabe|hier]] genommen und in meinen IDE importiert. Anschliessend habe ich die JavaDoc Dokumentation zu den Fachklassen und all deren Methoden hinzugefügt.

Hier sind die wichtigsten Tags die Javadoc hat:

|Javadoc-Tag|Verwendungszweck|Beispiel|
|---|---|---|
|`@param`|Beschreibt einen Methodeparameter|`@param username Der Benutzername, der übergeben wird.`|
|`@return`|Gibt den Rückgabewert einer Methode an|`@return Gibt den berechneten Durchschnitt zurück.`|
|`@throws`|Beschreibt mögliche Ausnahmen (Exceptions)|`@throws IllegalArgumentException Wenn der Wert negativ ist.`|
|`@see`|Verlinkt auf andere Klassen oder Methoden|`@see Math#sqrt(double)`|
|`@author`|Gibt den Autor oder die Autoren des Codes an|`@author Max Mustermann`|
|`@version`|Gibt die Version des Codes an|`@version 1.0`|
|`@since`|Gibt an, seit welcher Version verfügbar|`@since 2.0`|
|`@deprecated`|Markiert ein Element als veraltet|`@deprecated Verwenden Sie stattdessen die neue Methode xyz()`|