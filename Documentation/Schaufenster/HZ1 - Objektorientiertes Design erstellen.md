# HZ1 - Objektorientiertes Design erstellen

## Handlungsziel 

Analysiert Anwendungsprobleme zur Erstellung von objektorientierten Programmen 

## Theorie

| Theorie | Ressourcen | Zusammenfassung |
|---|---|---|
| Fachklassen | - [[01_Klassen und Objekte#Fachklassen]] <br> - [Webseite](https://www.programmierenlernenhq.de/klassen-in-java/) | Fachklassen sind im Prinzip Baupläne für Objekte die in der Starterklasse (also `public static void main(String[] args)` ) erstellt und verwendet werden können. Diese Objekte können eigene Variablen und Methoden haben. 
 |
| Kapselung | - [[02_Kapselung]] <br> - [Webseite](https://simpleclub.com/lessons/fachinformatikerin-datenkapselung-in-java) | Kapselung ist das zuweisen von Sichtbarkeit an die Variablen und Methoden einer Klasse. die Idee ist es die Variablen so zu schüzten, das sie nur durch eine `setter` Methode verändert werden kann, die evtl. den neuen Wert validieren kann oder ähnliches. Diese `setter` und `getter` Methoden sind deshalb öffentlich und können von allen aufgerufen werden. |


## Kompetenzen 

### A1G 

Ich kann aus einer einfachen Situationsbeschreibung auf mögliche Klassenkandidaten mit Attributen und Methoden schliessen.

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewisen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_1/AccountApplicationRefactored)
In diesem Auftrag musste ich Fachklassen aus der [[01_Klassen und Objekte#Refactoring AccountApplication|Situationsbeschreibung]] ableiten.
Die Situationsbeschreibung beschreibt die Entwicklung eines Programms, welches ein Konto erstellt und den Benutzer anschliessend nach Ein-/Auszahlungen fragt. Diese werden dann mit dem aktuellen Kontostand verrechnet. Dazu habe ich die Fachklasse in [account.java](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_1/AccountApplicationRefactored/account.java) erstellt und in das [main.java](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_1/AccountApplicationRefactored/main.java) implementiert.

Die Klasse Account hat eine Instanzvariable namens Balance, welche offensichtlich den aktuellen Kontostand speichert. Zudem hat die Klasse zwei Methoden, welche den Kontostand erhöhen oder senken. Zuletzt gibt es eine Getter-Methode, um den aktuellen Kontostandabzufragen. 


### A1F

Ich kann aus einer einfachen Situationsbeschreibung auf mögliche Klassenkandidaten mit Attributen und Methoden schliessen und stelle dabei Beziehungen unter den Klassen her. 

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewiesen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_2/SailingRace)

Hier in diesem Auftrag musste ich, wie in der [[05_Mehrere Fachklassen#Arbeitsblatt Regatta|Situationsbeschreibung]] gezeigt, eine Applikation entwickeln, welche für ein Schiffrennen simuliert. 
Dazu musste ich zwei Klassen erstellen. Diese sind zum einten die Klasse [Ship.java](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_2/SailingRace/Ship.java) und [Competition.java](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_2/SailingRace/Competition.java). Zudem gibt es natürlich noch die `main` Methode, in der die zwei Klassen kombiniert werden.
Die Klasse Ship soll ein Schiff in diesem Schiffrennen repräsentieren. Dazu hat die Klasse 3 Variablen. Zuerst ein ID um die Schiffe auseinander zu halten. Dann einen String für den Namen vom Schiff. Es gibt auch noch eine Variable für die Zeit die das Schiff im Rennen erreichte.
Diese Klasse hat einen Konstruktor, welcher die ID und den Namen des Schiffs setzt. Zudem gibt es setter- und getter-Methoden, für alle Variablen. DerSetter für die Zeit gibt es jedoch nicht. Dafür gibt es eine `Race` Methode, welche die Zeit zufällig auf einen Wert zwischen 300 und 600 setzt.
Anschliessend musste ich noch die Klasse `Competition` erstellen, diese hat die Variablen für einen Namen, welcher vom Konstruktor gesetzt wird und einen `Array` vom Typ `Ship` mit 5 Plätzen. Hier ist die Beziehung der zwei Klassen sichtbar. Die Methoden der Klasse sind folgende: 
Einen Konstruktor welcher, wie schon erwähnt, den Namen des Rennens setzt.
Eine getter-Methode für den Namen.
Eine Methode, um eine Schiff zum Rennen hinzuzufügen. Dies habe ich mit einer `for` Schleife erreicht.
Dann gibt es eine start Methode, welche alle Schiffe das Rennen fahren lässt.
Zuletzt gibt es eine Methode, welche das Resultat der Schiffe formatiert ausgibt.


### A1E

Ich kann komplexere Situationsbeschreibungen analysieren, Klassenkandidaten mit Attributen und Methoden definieren und in einer Vererbungshierarchie abbilden.

#### Umsetzung
Diese Kompetenz wurde in folgendem Auftrag bewiesen: [Link zum Code auf GitLab](https://gitlab.com/bbzbl_it/module_320/-/tree/main/Code/Woche_5/Polymorphie)

Dieser Auftrag war etwas schwieriger umzusetzen, doch das wir ihn in der Gruppe lösen mussten, machte es etwas einfacher. 

Die [[10_Polymorphie#Arbeitsblatt|Situationsbeschreibung]] beschreibt in diesem Auftrag eine Vererbungshirarchie welche eine Oberklasse [Auto.java](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_5/Polymorphie/Auto.java)  hat, welche von den Klassen [Gebrauchtwagen.java](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_5/Polymorphie/Gebrauchtwagen.java) und [VerunfaltesAuto.java](https://gitlab.com/bbzbl_it/module_320/-/blob/main/Code/Woche_5/Polymorphie/VerunfaltesAuto.java) erweitert wird. 
Die Klasse Auto besteht aus folgenden Variablen und Methoden:
Die Variablen `model`, `marke` und `price` sind offensichtlich was diese enthalten. 
Ein Konstruktor welcher genau diese 3 Variablen setzt. Dazu gibt es 3 getter für die 3 Variablen. Zumschluss gibt es noch zwei Methoden um den Preis des Autos je nach Schaden oder Meilenstand zu verändern.
Jetzt die "Unterklassen".
Anfangend mit dem Gebrauchtwagen. [Hier](https://gitlab.com/bbzbl_it/module_320/-/blame/main/Code/Woche_5/Polymorphie/Gebrauchtwagen.java#L1) kann man sehen wie die Klasse Gebrauchtwagen die Klasse Aute erweitert. Dies wird mit dem Keyword `extends` gemacht. Weiter unten sehen wir, das diese Klasse eine neue Variable namens `Meilenstand` einführt. Zudem hat diese Klasse auch einen eigenen Konstruktor. Dieser ist aber etwas anders aufgebaut, denn dieser verwendet das Keyword `super(xyz);` welcher [hier](https://gitlab.com/bbzbl_it/module_320/-/blame/main/Code/Woche_5/Polymorphie/Gebrauchtwagen.java#L6) zu finden ist. Dieses Keyword ist dazu da, um den Konstruktor der Oberklasse anzusprechen und die entsprechenden Werte mitzugeben. Zudem kann man [hier](https://gitlab.com/bbzbl_it/module_320/-/blame/main/Code/Woche_5/Polymorphie/Gebrauchtwagen.java#L8) einen weiteren Effekt der Vererbung erkennen, den es wird eine Methode aufgerufen, welche in dieser Klasse gar nicht definiert ist. Da wird die Methode einfach von der Oberklasse definiert. Ein anderes Problem der Vererbung kann man [hier](https://gitlab.com/bbzbl_it/module_320/-/blame/main/Code/Woche_5/Polymorphie/Gebrauchtwagen.java#L16) erkennen, den hier wird eine Methode definiert, welche in der Oberklasse auch schon definiert wurde, jedoch trotzdem angesprochen werden muss. Hierfür wird auch wieder das Keyword `super.METHOD();` verwendet, welches definiert, das die Methode die in der Oberklasse definiert ist, ausgeführt werden soll.
Weiter geht es mit der Klasse VerunfalltesAuto. Diese Klasse ist nicht gross anders wie meine Gebrauchtwagen Klasse, deshalb werde ich bei dieser nicht ins Detail gehen. der grösste Unterschied ist, das bei diesen Auto der preis des Autos vom dem grad des Schadens abhängt und nicht vom der gefahrenen Strecke.  

