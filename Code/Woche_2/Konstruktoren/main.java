import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		double amount = 0;
		String command = "";

		Scanner scanner = new Scanner(System.in);
		
		Account account = new Account();
		
		System.out.println("Welcome to the account application");
		
		
		do {
			System.out.println("\r\n");
			System.out.println("What do you want to do?\r\n"
						 + "1. Disposit\r\n"
						 + "2. Withdraw\r\n"
						 + "3. Get Balance\r\n"
						 + "----------------------------");
				command = scanner.next();
				
				if (!command.equals("3")) {
					System.out.println("Enter Amount:");
					amount = scanner.nextDouble();
				}
				
				switch(command) {
				  case "1":
				    account.deposit(amount);
				    break;
				  case "2":
					account.withdraw(amount);
				    break;
				  case "3":
					System.out.print("Balance is: ");
					System.out.println(account.getBalance());
				    break;
				  default:
				    System.out.println("INPUT ERROR: input is not valid\r\n"
				    		+ "exit");
				    System.exit(0);
				}
			} while (amount != 0);

		
		System.out.println("Final balance: " + account.getBalance());
		
		scanner.close();
	}

}
