public class Competition {

    private String name;
    private Ship[] ships = new Ship[5];
    
    public Competition(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addShip(Ship ship) {
        for (int i = 0; i < ships.length; i++) {
            if(ships[i] == null){
                ships[i] = ship;
                return;
            }
        }
    }

    public void start() {
        for (int i = 0; i < ships.length; i++) {
            if(ships[i] != null) {
                ships[i].race();
            }
        }
    }

    public void printResult() {
        System.out.println(" Results of the Race: ");
        System.out.println("----------------------");
        System.out.println(" Time | Number | Name ");
        System.out.println("----------------------");
        
        for (Ship ship : ships) {
            if (ship != null) {
                System.out.println(" " + ship.getTime() + " | " + ship.getNr() + " | " + ship.getName());        
            }
        }
        
        System.out.println("----------END---------");

    }
}