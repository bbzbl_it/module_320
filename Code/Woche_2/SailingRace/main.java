public class main {

    public static void main(String[] args) {
        Competition c = new Competition("Rotsee Regatta");
        Ship ship1 = new Ship(1, "Josh");
        Ship ship2 = new Ship(2, "Seth");
        Ship ship3 = new Ship(3, "Simon");
        c.addShip(ship1);
        c.addShip(ship2);
        c.addShip(ship3);
        c.start();
        c.printResult();
    }
}