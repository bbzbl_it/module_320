public class Gebrauchtwagen extends Auto{

    private double Meilenstand;

    public Gebrauchtwagen(String model, String marke, double Meilenstand, double price) {
        super(model, marke, price);
        this.Meilenstand = Meilenstand;
        setCarPriceMeilenstand(Meilenstand);
    }

    public void  setMeilenstand(double Meilenstand){
        this.Meilenstand = Meilenstand;
    }

    public double getCarPrice() {

        double priceOneKm = (this.price / 100) * 0.0005;
        // Preis - Abschreibung
        double newPrice = this.price - (priceOneKm * this.Meilenstand);
        if (newPrice < 0) {
            newPrice = 0;
        }
        return newPrice;
    }
    
    public double getMeilenstand(){
        return  Meilenstand;
    }
}