// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Gebrauchtwagen gebAuto = new Gebrauchtwagen("gagibubu", "BMW", 10000, 5000);
        VerunfaltesAuto verunfaltesAuto = new VerunfaltesAuto("gagibubu", "BMW", 10000, 2 );

        System.out.println(gebAuto.getMeilenstand() + " " + gebAuto.getCarPrice()+ " " + gebAuto.getMarke());
        System.out.println(verunfaltesAuto.getCarPrice() + verunfaltesAuto.getMarke());

        Fleet fleet = new Fleet();

        Gebrauchtwagen gebAuto2 = new Gebrauchtwagen("gagibubu", "BMW", 10000, 5000);
        fleet.AddCar(gebAuto2);
        fleet.PrintList();
    }
}