public class Axe implements IWeapon{
    private int maxDamage;

    public Axe(int ap) {
        this.maxDamage = ap;
    }

    public double use() {
        double attackMultiplyer = Math.random();

        return maxDamage * attackMultiplyer;
    }

    public String getName() {
        return "Axe";
    }
}
