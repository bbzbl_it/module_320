public interface IWeapon {
    double use();
    String getName();
}
