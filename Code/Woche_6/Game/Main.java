public class Main {
    public static void main(String[] args) {

        Character char1 = new Character("Josh", 100, 20);
        Character char2 = new Character("Simon", 100, 20);

        Fight fight = new Fight(char1, char2);
        
        System.out.println("Spiel wird mit folgenden Charakteren gestartet:");
        System.out.println(char1.getName() + ":  HP:" + char1.getHp() + "  Weapon:" + char1.getWeapon().getName());
        System.out.println(char2.getName() + ":  HP:" + char2.getHp() + "  Weapon:" + char2.getWeapon().getName());

        fight.fight();

        System.out.println(fight.getWinner().getName() + " hat " + fight.getLooser().getName() + " besiegt.");

    }
}