public class Character {

	private String name;
	private int hp; // health
	private IWeapon Weapon; // attack
	private int maxHp;
	
	public Character(String name, int hp, int ap) {
		this.name = name;
		this.hp = hp;
		this.maxHp = hp;
		setWeapon(ap);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHp() {
		return hp;
	}

	private void setHp(int hp) {
		if(hp < 0) {
			this.hp = 0;
		} else {
			this.hp = hp;
		}
	}

	public IWeapon getWeapon() {
		return this.Weapon;
	}

	
	public void heal() {
		this.hp = this.maxHp;
	}
	public void attack(Character enemy) {
		int hit = (int) this.Weapon.use();
		enemy.setHp(enemy.getHp() - hit);
		
		System.out.println(this.name + " hit " + enemy.name + " with " + hit);
		this.log(this);
		this.log(enemy);
	}
	
	private void log(Character character) {
		System.out.println(character.getName() +": " + "hp=" + character.getHp());
	}

	private void setWeapon(int ap) {
		switch ((int) (Math.random() * 2) + 1) {
			case 1:
				this.Weapon = new Dagger(ap);
				break;
			case 2:
				this.Weapon = new Axe(ap);
				break;
		}
	}
}