public class Dagger implements IWeapon{
    private int maxDamage;


    public Dagger(int ap) {
        this.maxDamage = ap / 2;
    }


    public double use() {
        double attackMultiplyer = Math.random() + Math.random();

        return maxDamage * attackMultiplyer;
    }

    public String getName() {
        return "Dagger";
    }
}
