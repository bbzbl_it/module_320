import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Klasse, um Wettkämpfe mit Schiffen zu absolvieren
 * 
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class Competition {

	private String name;
	private ArrayList<Ship> ships = new ArrayList<Ship>();

	/**
	 * Erstellt ein neues Wettkampf Objekt
	 * 
	 * @param name Name des Wettkampfs
	 */
	public Competition(String name) {
		this.name = name;
	}

	/**
	 * Startet den Wettkampf.
	 * 
	 * @see Ship#race()
	 */
	public void start() {

		for (Ship s : this.ships) {
			if (s != null) {
				s.race();
			}
		}

		// Alternative:
		/*
		 * for (int i = 0; i < ships.length; i++) { if (ships[i] != null) { Ship s =
		 * ships[i]; s.race(); } }
		 */
	}

	/**
	 * Fügt ein Schiff zum Wettkampf hinzu, sofern noch nicht alle Plätze belegt
	 * sind.
	 * 
	 * @param s Ship
	 */
	public void addShip(Ship ship) {
		this.ships.add(ship);
	}

	/**
	 * Gibt alle Schiffe des Wettkampfs auf die Konsole aus
	 */
	public void printResult() {
		System.out.println("Wettkampf: " + this.name);

		this.ships = sortShips(this.ships);

		int place = 1;

		for (Ship s : this.ships) {
			String ownerOutput = " || Besitzer: ";

			Person owner = s.getOwner();
			if (owner != null) {
				ownerOutput += owner.getFirstname() + " " + owner.getLastname();
			}

			System.out.println(
					"Plazierung: " + place + " || Schiff Nr: " + s.getNr() + " || Name: " + s.getName() + ownerOutput + " ||  Zeit: " + s.getTime());
			place++;
		}		
	}

	private ArrayList<Ship> sortShips(ArrayList<Ship> ships) {
		ArrayList<Ship> sortedShips = new ArrayList<>(ships);

		Collections.sort(sortedShips, new Comparator<Ship>() {
			@Override
			public int compare(Ship ship1, Ship ship2) {
				return Integer.compare(ship1.getTime(), ship2.getTime());
			}
		});

		return sortedShips;
	}
}
