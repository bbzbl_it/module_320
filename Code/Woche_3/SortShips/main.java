/**
 * Testklasse
 * @author Roger Zaugg
 * @version 1.0
 *
 */
public class main {

	/**
	 * Startmethode
	 * @param args
	 */
	public static void main(String[] args) {
		
		Competition c = new Competition("Rotsee Regatta");
		
		Person owner = new Person("Roger", "Zaugg", "Hölstein");
		Ship ship1 = new Ship(1, "Alinghi");
		Ship ship2 = new Ship(2, "Red Baron");
		Ship ship3 = new Ship(3, "Blue Lagoon");
		
		ship1.setOwner(owner);
		ship2.setOwner(owner);
		ship3.setOwner(owner);
		
		c.addShip(ship1);
		c.addShip(ship2);
		c.addShip(ship3);
		
		c.start();
	
		c.printResult();
	}	
}
