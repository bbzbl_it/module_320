public class main {
    public static void main(String[] args) {
        
        Invoice invoice1 = new Invoice(1);

        InvoiceItem item11 = new InvoiceItem(10.51, "Döner");
        InvoiceItem item12 = new InvoiceItem(0.55, "Energy");
        InvoiceItem item13 = new InvoiceItem(7.8, "Magenbrot");

        invoice1.addItem(item11);
        invoice1.addItem(item12);
        invoice1.addItem(item13);

        System.out.println(invoice1.getTotal());

        invoice1.print();

    }
}
