public class InvoiceItem {
    
    private double price;
    private String description;


    public InvoiceItem(double price, String description) {
        this.price = price;
        this.description = description;
    }

    public double getPrice() {
        return price;
    }


    public String getDescription() {
        return description;
    }

}
