import java.util.ArrayList;

public class Invoice {
    
    private int number;
    private ArrayList<InvoiceItem> items = new ArrayList<InvoiceItem>();

    
    public Invoice(int number) {
        this.number = number;
    }

    public double getTotal(){
        int total = 0;

        for (InvoiceItem item : items) {
            total += item.getPrice();
        }

        return total;
    }

    public int getNumber() {
        return number;
    }

    public void addItem(InvoiceItem Item) {
        this.items.add(Item);
    }
    
    public InvoiceItem getItem(int index) {
        return this.items.get(index);
    }

    public ArrayList<InvoiceItem> getItems() {
        return this.items;
    }

    public void setItem(int index, InvoiceItem item) {
        this.items.set(index, item);
    }

    public void removeItem(InvoiceItem item) {
        this.items.remove(item);
    }

    public void removeItem(int index) {
        this.items.remove(index);
    }

    public void print() {
        int position = 1;

        System.out.println("Print Invoice:");
        System.out.println("--------------");

        System.out.println("Invoice Number: " + this.getNumber());
        System.out.println("");
        System.out.println("Pos.\t\tPrice\t\t\tDescription");
        System.out.println("----------------------------------------------------------------");

        for (InvoiceItem item : this.items) {
            System.out.println(position + "\t\t" + item.getPrice() + " CHF \t\t" + item.getDescription());
            position++;
        }

        System.out.println("----------------------------------------------------------------");

        System.out.println("\tTotal: \t" + this.getTotal() + "CHF");
        System.out.println("----------------------------------------------------------------");
    }
}
