public class Ship {

	private int nr;
	private String name;
	private int time;

	/**
	 * @description Ship Creates a Ship Instance
	 * @param  nr   nr of the Ship
	 * @param  name name of the Ship
	 */
	public Ship(int nr, String name) {
		this.nr = nr;
		this.name = name;
	}

	/**
	 * @description Return nr of Ship
	 * @return   return nr of Ship
	 */
	public int getNr() {
		return nr;
	}

	
	/**
	 * @description Set nr of the Ship
	 * @param  nr nr of the Ship
	 */
	public void setNr(int nr) {
		this.nr = nr;
	}

	/**
	 * @description Return nr of Ship
	 * @return   return nr of Ship
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @description Set name of the Ship
	 * @param  name name of the Ship
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @description Race the Ship, Set rand time betw. 300-600
	 */
	public void race() {
		int min = 300;
		int max = 600;
		this.time = (int) (Math.random() * (max - min + 1)) + min;
	}
	
	/**
	 * @description Return Time of Ship
	 * @return   return time of Ship
	 */
	public int getTime() {
		return time;
	}
	
}
