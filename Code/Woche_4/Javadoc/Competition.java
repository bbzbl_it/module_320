
public class Competition {

	private String name;
	private Ship[] ships = new Ship[5];

	/**
	 * @description Creates a new Competition Instance
	 * @param  name name of the Competition
	 */
	public Competition(String name) {
		this.name = name;
	}

	/**
	 * @description start the Competition
	 */
	public void start() {

		for (Ship s : this.ships) {
			if (s != null) {
				s.race();
			}
		}
	}

	/**
	 * @description Add a Ship to the Competition
	 * @param  ship ship to add
	 */
	public void addShip(Ship ship) {
		for (int i = 0; i < ships.length; i++) {
			if (ships[i] == null) {
				ships[i] = ship;
				break;
			}
		}
	}

	/**
	 * @description Print the result of the Competition
	 */
	public void printResult() {
		System.out.println("Wettkampf: " + this.name);

		for (Ship s : this.ships) {
			if (s != null) {
				System.out.println("Schiff Nr: " + s.getNr() + " Name: " + s.getName() + " Zeit: " + s.getTime());
			}
		}
	}
}
