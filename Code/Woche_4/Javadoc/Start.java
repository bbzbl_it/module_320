
public class Start {

	
	public static void main(String[] args) {
		
		// Erstelt eine neues Objekt vom typ Competition
		Competition c = new Competition("Rotsee Regatta");
		
		// Erstelle die Schiffe für die Competition 
		Ship ship1 = new Ship(1, "Alinghi");
		Ship ship2 = new Ship(2, "Red Baron");
		Ship ship3 = new Ship(3, "Blue Lagoon");
		
		// Fügt die Schiffe zu der Competition hinzu
		c.addShip(ship1);
		c.addShip(ship2);
		c.addShip(ship3);
		
		// Started die Competition
		c.start();
	
		// Gibt das resultat aus
		c.printResult();
	}	
}