package account;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountTest {

	private Account account = null;
	private double initialBalance;
	
	@BeforeEach
	void setUp() throws Exception {
		initialBalance = ((int) (Math.random() * 1000000)) / 100.0;
		account = new Account(initialBalance);
	}

	@AfterEach
	void tearDown() throws Exception {
		account = null;
	}

	@Test
	void testGetBalance() {
		assertEquals(initialBalance, account.getBalance());
	}

	@Test
	void testWithdraw() {
        HashMap<Double, Boolean> hashMap = new HashMap<>();
        double initialBalance = 1000.00;

        hashMap.put(initialBalance, true);
        hashMap.put(0.0, true);
        hashMap.put(-0.01d, true);
        hashMap.put(initialBalance + 0.01, false);
        hashMap.put(initialBalance - ((int) Math.random() * 10000) / 100, true);

        for (Double key : hashMap.keySet()) {
            account = new Account(initialBalance);
		
			assertEquals(hashMap.get(key) ,account.withdraw(key));
		}
	}

	@Test
	void testDeposit() {
		double depositAmmount = ((int) Math.random() * 10000) / 100;
		
		account.deposit(depositAmmount);
		
		assertEquals(initialBalance + depositAmmount, account.getBalance());
	}

}
