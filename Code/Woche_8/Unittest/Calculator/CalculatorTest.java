package calculator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculatorTest {

	private Calculator calculator = null;

	private int nr1 = 0;
	private int nr2 = 0;
	
	@BeforeEach
	void setUp() throws Exception {
		calculator = new Calculator();

		nr1 = (int) (Math.random() * 100);
		nr2 = (int) (Math.random() * 100);
	}

	@AfterEach
	void tearDown() throws Exception {
		calculator = null;

		nr1 = 0;
		nr2 = 0;
	}

	@Test
	void testAdd() {		
		assertEquals(nr1 + nr2, calculator.add(nr1, nr2));
	}

	@Test
	void testSubstract() {
		assertEquals(nr1 - nr2, calculator.substract(nr1, nr2));
	}

}
