public class Auto {

    private String model;

    private String marke;

    protected double price;

    public Auto(String model, String marke, double price){
        this.model  = model;
        this.marke  = marke;
        this.price = price;
    }

    protected double getCarPrice() {
        return this.price;
    }

    public void setCarPriceToDamageLvL( int dmgLvl){
        this.price = this.price - ((this.price / 100 ) * (dmgLvl * 20));
    }

    public void setCarPriceMeilenstand( double meilenstand){
        this.price = this.price - ((this.price / 100 ) * (meilenstand * 0.0005));
    }

    public String getMarke(){
        return  this.marke;
    }

    public String getModel(){
        return  this.model;
    }
}
