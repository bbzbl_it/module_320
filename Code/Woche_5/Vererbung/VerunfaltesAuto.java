public class VerunfaltesAuto extends Auto{

    private int DamageLvL;

    // an enum would be nicer
    public static final int DMG_LVL_LOW = 0;
    public static final int DMG_LVL_MEDIUM = 1;
    public static final int DMG_LVL_TOTAL = 2;

    public VerunfaltesAuto(String model, String marke, double price, int DamageLvl) {
        super(model, marke, price);
        this.DamageLvL = DamageLvl;
        setCarPriceToDamageLvL(DamageLvl);
    }

    public double getCarPrice() {
        double pr = this.price;
        switch (this.DamageLvL) {
            case DMG_LVL_LOW:
                pr *= 0.9;
                break;
            case DMG_LVL_MEDIUM:
                pr *= 0.5;
                break;

            case DMG_LVL_TOTAL:
                pr = 0;
                break;

        }
        return pr;
    }

    public int getDamageLvL(){
        return this.DamageLvL;
    }

    public void setDamageLvL(int DamageLvL){
        this.DamageLvL = DamageLvL;
    }

}
