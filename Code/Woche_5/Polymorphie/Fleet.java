import java.util.ArrayList;
import java.util.List;

public class Fleet {

    private List<Auto> list = new ArrayList<>();

    public void PrintList(){
        for (int i = 0; i < list.size(); i++) {
            Auto auto = list.get(i);
            System.out.println(auto.getMarke() + " " + auto.getModel() + " " + auto.getCarPrice() );
        }
    }

    public void  AddCar(Auto auto){
        list.add(auto);
    }
}
