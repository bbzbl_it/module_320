public class Gebrauchtwagen extends Auto{

    private double Meilenstand;

    public Gebrauchtwagen(String model, String marke, double Meilenstand, double price) {
        super(model, marke, price);
        this.Meilenstand = Meilenstand;
        setCarPriceMeilenstand(Meilenstand);
    }

    public void  setMeilenstand(double Meilenstand){
        this.Meilenstand = Meilenstand;
    }

    public double getCarPrice(){
       return super.getCarPrice();
    }

    public double getMeilenstand(){
        return  Meilenstand;
    }
}