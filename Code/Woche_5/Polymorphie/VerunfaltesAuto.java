public class VerunfaltesAuto extends Auto{

    private int DamageLvL;

    public VerunfaltesAuto(String model, String marke, double price, int DamageLvl) {
        super(model, marke, price);
        this.DamageLvL = DamageLvl;
        setCarPriceToDamageLvL(DamageLvl);
    }

    protected double getCarPrice() {
        return super.getCarPrice();
    }

    public int getDamageLvL(){
        return this.DamageLvL;
    }

    public void setDamageLvL(int DamageLvL){
        this.DamageLvL = DamageLvL;
    }

}
